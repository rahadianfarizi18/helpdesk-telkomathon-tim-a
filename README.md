# SOLVER - Bridging Questions to Answers
Project ini dibangun menggunakan [reactjs](https://reactjs.org), dengan fitur:
- Customized Helpdesk System (Admin): Manage Ticket, Manage User, Manage Categories, Instant Notification
- Easy to Use Ticketing Apps: Creating Ticket, Tracking Ticket, Instant Notification, FAQ

# WHY
Karyawan TelkomGroup sering kali dibingungkan bagaimana mencari informasi tentang berbagai program yang ada di internal Telkom, seperti : TelkomAthon, Amoeba, Inkubasi, hingga IT Infrastructure.
Belum tersedia sebuah sistem terintegrasi dimana karyawan bisa menanyakan permasalahannya ke PIC yang dituju lewat satu pintu.
Karyawan harus proaktif mencari tau masing-masing PIC, atau minimal mengeksplor di web masing-masing program.

# BUILT WITH
- React-Bootstrap (https://react-bootstrap.github.io)
- Redux (https://redux-toolkit.js.org)
- Redux Thunk (https://github.com/reduxjs/redux-thunk)
- redux-persist (https://github.com/rt2zz/redux-persist)
- react-data-table-component (https://react-data-table-component.netlify.app)
- Font Awesome (https://fontawesome.com)

# INSTALLATION
- Clone to local
- Make sure you have installed [npm](https://www.npmjs.com)
- Install packages using `npm install`
- Run on local using `npm run dev`

# DEPLOYMENT
You can see deployed version of the web on https://solver-athon.herokuapp.com

# AUTHOR
**Team A - Stream Front End Telkom Athon 2**
- Rahadian Farizi
- Fijrah Asri
- Sudarman Hardono Manik

