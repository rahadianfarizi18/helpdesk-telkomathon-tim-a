//modules
import { Col, Container, Row, Form, Button, Modal, Spinner, Alert } from "react-bootstrap";
import { Link, useHistory, useLocation } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { useState, useEffect, useMemo } from "react";
import { get, lowerCase } from 'lodash';
import DataTable from "react-data-table-component";
import numeral from 'numeral'
//redux
import { fetchGroups } from "../redux/users/action";
import { fetchStatusTicketRefApi, fetchTicketApi, updateTicketApi } from "../redux/tickets/action";
import { manageTicketTableStyles } from "../assets/js/tableStyles";
//components
import Header from "../components/Header";
import Footer from "../components/Footer";
//styles & assets
import styles from '../assets/scss/manageTickets.module.scss'
import illustration from '../assets/images/manage-tickets.png'
import ModalNotification from "../components/ModalNotification";


const ManageTicket = () => {

    const dispatch = useDispatch()
    const history = useHistory()
    const location = useLocation()

    const [filter, setFilter] = useState(get(location.state, 'filterTickets', {}))
    const [isTicketsDataLoading, setIsTicketsDataLoading] = useState(false)
    const [isLoading, setIsLoading] = useState(false)
    const [showModalProcessTicket, setShowModalProcessTicket] = useState(false)
    const [showModalAssignGroup, setShowModalAssignGroup] = useState(false)
    const [showModalSuccessAssignGroup, setShowModalSuccessAssignGroup] = useState(false)
    const [showAlert, setShowAlert] = useState(false)
    const [errorMessage, setErrorMessage] = useState(false)
    const [ticketUpdate, setTicketUpdate] = useState({})
    const [ticketUpdateGroup, setTicketUpdateGroup] = useState({})
    const [categoryToAssign, setCategoryToAssign] = useState('')
    
    const groups = useSelector((states) => states.users.groups)
    const stats = useSelector((states) => states.tickets.stats)
    const tickets = useSelector((states) => states.tickets.ticketsData)
    const [filteredTickets, setFilteredTickets] = useState(tickets)
    const countTickets = useSelector(states => states.tickets.allTicketsCount)
    const {token, role, profile, userId } = useSelector(states => states.auth)

    const handleChangeFilter = (e) => {
        setFilter({
            ...filter,
            [e.target.id]: e.target.value
        })
    }

    const handleProcessTicket = (mainTicketId) => {
        const data = {
            mainTicketId,
            assignedUserId: userId,
            statusTicket: 'On Progress'
        }
        setTicketUpdate(data)
        

        setShowModalProcessTicket(true)
    }

    const handleConfirmProcessTicket = () => {
        
        const fetchTickets = async () => {
            await dispatch(fetchTicketApi({token}))
            .then(() => setIsTicketsDataLoading(false))
            .catch(err => { 
                setErrorMessage(`Get ticket failed. ${err.message}`)
                setShowAlert(true)
            })
        }
        setIsLoading(true)
        dispatch(updateTicketApi({token, data: ticketUpdate}))
        .then((res) => {
            setIsLoading(false)
            fetchTickets()
            handleCloseModal()
        })
        .catch((err) => {
            setErrorMessage(err.message)
            setShowAlert(true)
            setIsLoading(false)
            handleCloseModal()
        })
    }

    const handleAssignGroup = (e, ticket) => {
        setTicketUpdateGroup(ticket)
        setShowModalAssignGroup(true)
    }

    const handleConfirmAssignGroup = async() => {

        const fetchTickets = async () => {
            setIsTicketsDataLoading(true)
            await dispatch(fetchTicketApi({token}))
            .then(() => setIsTicketsDataLoading(false))
            .catch(err => { 
                setErrorMessage(`Get ticket failed. ${err.message}`)
                setShowAlert(true)
                setIsTicketsDataLoading(false)
            })
        }

        setIsLoading(true)
        const data = {
            mainTicketId: ticketUpdateGroup.mainTicketId,
            category: categoryToAssign,
            statusTicket: 'Open'
        }
        dispatch(updateTicketApi({token, data}))
        .then(res => {
            if (res.code !== 'success') {
                setShowModalAssignGroup(false)
                setErrorMessage(res.message)
                setShowAlert(true)
                setIsLoading(false)
            } else {
                setShowModalAssignGroup(false)
                setShowModalSuccessAssignGroup(true)
                setIsLoading(false)
                fetchTickets()
            }
        })
        .catch(err => {
            setShowModalAssignGroup(false)
            setErrorMessage(err.message)
            setShowAlert(true)
            setIsLoading(false)
        })
    }

    const handleCloseModal = () => {
        setShowModalProcessTicket(false)
        setShowModalAssignGroup(false)
        setShowModalSuccessAssignGroup(false)
    }

    const handleDetailTicket = (e, mainTicketId) => {
        e.preventDefault();
        const location = {
            pathname: '/resolve-ticket',
            state: { mainTicketId }
        }
        history.push(location)
    }


    const manageButton = (ticket) => {
        try {
            if(ticket.statusTicket.toLowerCase() === 'open' && !!!ticket.assignedUser) {
                return <Button size='sm' variant='orange' className={styles.buttonManage} onClick={() => handleProcessTicket(ticket.mainTicketId)}>
                            Process ticket
                        </Button>
            } else if (ticket.statusTicket.toLowerCase() === 'dispatched') {
                return <Button size='sm' variant='yellow' className={styles.buttonManage} onClick={(e) => handleAssignGroup(e, ticket)}>
                           Assign to group
                        </Button>
            } else {
                return <Button size='sm' variant='black' className={styles.buttonManage} onClick={(e) => handleDetailTicket(e, ticket.mainTicketId)}>Go to ticket</Button>
            }
        }
        catch(err) {
            return  <Button size='sm' variant='black' className={styles.buttonManage} onClick={(e) => handleDetailTicket(e, ticket.mainTicketId)} >Go to ticket</Button>
        }
    }

    const tableHeader = [
        {
            name: 'Date',
            selector: row => row.createdAt,
            format: row => {
                const date = new Date(row.createdAt)
                return `${date.getDate()}/${numeral(date.getMonth()+1).format('00')}/${date.getFullYear()}`
            },
            sortable: true,
            maxWidth: '110px',
            style: {
                justifyContent: 'flex-end'
            }
        },
        {
            name: 'Ticket ID',
            selector: row => row.mainTicketId,
            sortable: true,
            // minWidth: '73px',
            width: '12%',
            wrap: true,
            style: {
                justifyContent: 'flex-end'
            }
        },
        {
            name: 'Status',
            selector: row => row.statusTicket,
            sortable: true,
            width: '15%',
            wrap: true,
        },
        {
            name: 'Group',
            selector: row => !!row.category ? row.category : <div className='text-muted fst-italic align-middle'>Not assigned</div> ,
            sortable: true,
            wrap: true,
        },
        {
            name: 'Agent',
            selector: row => !!row.assignedUser ? row.assignedUser.assignedUsername : <div className='text-muted fst-italic align-middle'>Not assigned</div>,
            sortable: true,
            wrap: true,
        },
        {
            name: 'Manage',
            selector: row => manageButton(row),
            allowOverflow: true,
            grow: 1,
            center: true,
        }
    ]

    // useMemo to filter tickets by search input
    useMemo(() => {
        setFilteredTickets (
            tickets.filter((ticket, i) => {
                const ticketId = ticket.mainTicketId
                const ticketGroup = ticket.category ? ticket.category : ''    
                const assignedAgent = !!ticket.assignedUser ? ticket.assignedUser.assignedUsername : ''
    
                // id search bar can also search assigned agent for super agent
                const ticketIdRes =
                            filter.id ? 
                                ticketId === Number(filter.id) ||
                                (role === 'Super Agent' && assignedAgent.toLowerCase().includes(filter.id.toLowerCase()))
                                :
                                true

                let statusFilterRes
                try {
                    const ticketStatus = lowerCase(ticket.statusTicket)
                    statusFilterRes = filter.status && filter.status !== 'All' ? ticketStatus.includes(lowerCase(filter.status)) : true
                }
                catch {
                    statusFilterRes = !filter.status || filter.status === 'All'
                }
                const groupFilterRes = filter.group && filter.group !== 'All' ? ticketGroup === filter.group : true
                return (
                    ticketIdRes && statusFilterRes && groupFilterRes
                )})
        )
    }, [filter.group, filter.id, filter.status, role, tickets])
    
    useEffect(() => {

        const fetchTickets = async () => {
            setIsTicketsDataLoading(true)
            await dispatch(fetchTicketApi({token}))
            .then(() => setIsTicketsDataLoading(false))
            .catch(err => { 
                setErrorMessage(`Get ticket failed. ${err.message}`)
                setShowAlert(true)
                setIsTicketsDataLoading(false)
            })
        }

        const fetchStatusTicket = async () => {
            await dispatch(fetchStatusTicketRefApi(token))
            .catch(err => {
                setErrorMessage(`Get status ticket ref failed. ${err.message}`)
                setShowAlert(true)
            })
        }

        const fetchGroupsFunc = async () => {
            dispatch(fetchGroups(token))
            .catch(err => {
                setErrorMessage(`Get category ref failed. ${err.message}`)
                setShowAlert(true)
            })
            
        }

        fetchTickets()
        fetchStatusTicket()
        fetchGroupsFunc()


    }, [countTickets, dispatch, role, token, userId])

    return (
        <>
            <header>
                <Header />
            </header>
            <body className={styles.body}>
                <Container>
                    <Alert show={showAlert} variant='danger' id='fail-get-ticket'>{errorMessage}</Alert>
                    <Row>
                        <div className={`h3 ${styles.title}`}>Manage Tickets</div>
                    </Row>
                    <Row className={styles.filterRoot}>
                        <Col xs='6' lg='2' className={styles.filter}>
                            <Form.Label>
                                {role === 'Super Agent' ? 'Ticket ID/username' : 'Ticket ID'}
                            </Form.Label>
                            <Form.Control
                                id='id'
                                className={`input-text ${styles.inputFilter}`}
                                placeholder= {role === 'Super Agent' ? 'enter ticket ID or agent' : 'enter ticket ID'}
                                onChange={(e) => handleChangeFilter(e)}
                                value={filter.id}
                            />
                            {role === 'Super Agent' ?
                                <Form.Text onClick={() => setFilter({...filter, id: profile.username})}><Link>Show my tickets</Link></Form.Text>
                                :
                                <></>
                            }
                        </Col>
                        <Col xs='6' lg='2' className={styles.filter}>
                            <Form.Label>Status</Form.Label>
                            <Form.Select 
                                id='status'
                                className={`input-select ${styles.inputFilter}`}
                                onChange={(e) => handleChangeFilter(e)}
                            >
                                <option>All</option>
                                {stats.map((stat, i) => (
                                    <option selected={filter.status === stat.label} key={i}>{stat.label}</option>
                                    )
                                )}
                            </Form.Select>
                        </Col>
                        <Col xs='6' lg='2' className={styles.filter}>
                            { role.replace('-', ' ').toLowerCase() === 'super agent' ? 
                                <>
                                    <Form.Label>Group</Form.Label>
                                    <Form.Select id='group' className={`input-select ${styles.inputFilter}`} onChange={(e) => handleChangeFilter(e)} >
                                        <option>All</option>
                                        {groups.map((group, i) => (
                                            <option key={i}>{group.label}</option>
                                        ))}
                                    </Form.Select>
                                </>
                                :
                                <></>
                            }
                        </Col>
                    </Row>
                    <Row>
                        <Col sm={12} lg={9}>
                            <Row>
                                {isTicketsDataLoading ? 
                                    <div className='d-flex justify-content-center'>
                                        <Spinner className='spinner-border m-5 text-center' variant='primary'>
                                            <span className='visually-hidden'></span>
                                        </Spinner> 
                                    </div>
                                    :
                                    <div className={styles.tableWrapper}>
                                        <DataTable
                                            responsive
                                            fixedHeader
                                            fixedHeaderScrollHeight = '52vh'
                                            columns={tableHeader}
                                            data = {filteredTickets}
                                            customStyles = {manageTicketTableStyles}
                                            dense
                                            highlightOnHover
                                            pagination
                                            paginationComponentOptions={{
                                                selectAllRowsItem: true
                                            }}
                                        />
                                    </div>
                                }
                            </Row>
                        </Col>
                        <Col lg={3} sm={0} className={styles.colImg}>
                            <img alt='illustration' src={illustration} height='70%' />
                        </Col>
                    </Row>
                </Container>
                <Modal id='modal-process-ticket' centered show={showModalProcessTicket} onHide={handleCloseModal} contentClassName={styles.modalPaper}>
                    <Modal.Header className='modal-header-custom' closeButton />
                    <Modal.Body>
                        <p>You will be assigned to handle this group. Continue?</p>
                    </Modal.Body>
                    <Modal.Footer className='modal-footer-custom'>
                        <Button size='sm' variant='orange' className='btn-modal' onClick={handleConfirmProcessTicket}>
                            {isLoading ?
                                (
                                    <>
                                    <span className='spinner-border spinner-border-sm'></span>
                                    {'  '}Loading...
                                    </>
                                ) :
                                "Yes"
                            }
                        </Button>
                        <Button size='sm' className='btn-modal' onClick={handleCloseModal}>No</Button>
                    </Modal.Footer>
                </Modal>
                <Modal id='modal-assign-group' centered show={showModalAssignGroup} onHide={handleCloseModal} contentClassName={styles.modalPaperYellow}>
                    <Modal.Header className='modal-header-custom' closeButton />
                    <Modal.Body className={styles.modalGroupBody}>
                            <p className={styles.modalGroupTitle}>Assign to a Group</p>
                            <p className={styles.modalGroupText}>Problem: {ticketUpdateGroup.description}</p>
                            <p className={styles.modalGroupText}>Additional Info: {ticketUpdateGroup.additionalInfo}</p>
                            <Form.Select className={styles.modalGroupSelect} defaultValue='Select' onChange={(e) => setCategoryToAssign(e.target.value)}>
                                <option disabled>Select</option>
                                {groups.map((group, i) => (
                                    <option key={group.categoryId}>{group.category}</option>
                                ))}
                            </Form.Select>
                    </Modal.Body>
                    <Modal.Footer className='modal-footer-custom'>
                        <Button size='sm' className='btn-modal' onClick={handleConfirmAssignGroup}>
                            {isLoading ?
                                (
                                    <>
                                    <span className='spinner-border spinner-border-sm'></span>
                                    {'  '}Loading...
                                    </>
                                ) :
                                "Assign"
                            }
                        </Button>
                    </Modal.Footer>
                </Modal>
                <ModalNotification
                    showModal={showModalSuccessAssignGroup}
                    handleCloseModal={() => handleCloseModal()}
                    message={
                        <>
                        Ticket <span className={styles.modalSuccessAsignText}>{ticketUpdateGroup.mainTicketId}</span> is assigned to <span className={styles.modalSuccessAsignText}>{categoryToAssign}</span>
                        </>
                    }
                />
            </body>
            <Footer />
        </>
    )
}

export default ManageTicket;