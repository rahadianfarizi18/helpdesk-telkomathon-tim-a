//modules
import { useState, useEffect } from "react"
import { Container, Card, Col, Row, Alert } from "react-bootstrap"
import { useDispatch, useSelector } from "react-redux"
import { useHistory, Link } from "react-router-dom"
import { defaultTo } from "lodash"
//redux
import { countTicketByStatus } from "../redux/tickets/action"
//components
import Header from "../components/Header"
import Footer from '../components/Footer'
//styles & assets
import '../assets/scss/card.scss'
import images from '../assets/images/home.png'
import logo from '../assets/logo/logo-solver.png'

const Home = () => {
    const dispatch = useDispatch()
    const history = useHistory()

    const {token, role, userId} = useSelector(states => states.auth)
    const countTicketsByStatus = useSelector(states => defaultTo(states.tickets.countTicketsByStatus, {}))

    const [isLoading, setIsLoading] = useState(false)
    const [errorMessage, setErrorMessage] = useState(false)
    const [showAlert, setShowAlert] = useState(false)

    const handleClickFilterTicket = (e) => {
        e.preventDefault();
        const locationTo = {
            pathname: '/manage-tickets',
            state: {
                filterTickets: {
                    status: e.target.id
                }
            }
        }
        history.push(locationTo)
    }

    useEffect(() => {

        const countTicketsByStatusFunc = async () => {
            setIsLoading(true)
            dispatch(countTicketByStatus({token, role, userId}))
            .then(() => setIsLoading(false))
            .catch(err => { 
                setErrorMessage(`Get ticket failed. ${err.message}`)
                setShowAlert(true)
                setIsLoading(false)
            })
        }

        countTicketsByStatusFunc()
    }, [dispatch, role, token, userId])

    return(
        <>
            <header>
                <Header />
            </header>
            <body className='body-home-page'>
                <Container>   
                    <Alert show={showAlert} variant='danger' id='fail-get-ticket'>{errorMessage}</Alert>
                    <Row>
                    <Col sm='12' padding='10px' lg='5' md='12' className='card-body-wrapper'>
                    <Row>
                        <Col xs='6'>
                            <Card className='card-ticket'>
                                <Card.Header className= 'card-header'>Open Tickets</Card.Header>
                                    <Link style={{textDecoration: 'none'}} >
                                        <Card.Text id='Open' onClick={(e) => handleClickFilterTicket(e)} >
                                            {isLoading ?
                                                (
                                                    <>
                                                    <div class="spinner-border card-loading-home-page" role="status">
                                                        <span class="visually-hidden">Loading...</span>
                                                    </div>
                                                    </>
                                                ) 
                                                :
                                                countTicketsByStatus.open + countTicketsByStatus.reopen
                                            }
                                        </Card.Text>
                                    </Link>
                            </Card>
                        </Col>
                        <Col xs='6'>
                            <Card className='card-ticket card-definition'>
                                <div className='card-definition-text'>
                                Open tickets are new tickets created by the user or solved tickets that has been 
                                re-opened by the user that has not yet being handled by any agent.
                                </div>
                            </Card>
                        </Col>
                    </Row>

                    {role.replace('-', ' ').toLowerCase() === 'super agent' ? 
                    <Row>
                        <Col xs='6'>
                            
                            <Card className='card-ticket'>
                                <Card.Header className= 'card-header'>Dispatched Tickets</Card.Header>
                                    <Link style={{textDecoration: 'none'}} >
                                        <Card.Text id='Dispatched' onClick={(e) => handleClickFilterTicket(e)}>                         
                                            {isLoading ?
                                                (
                                                    <>
                                                    <div class="spinner-border card-loading-home-page" role="status">
                                                        <span class="visually-hidden">Loading...</span>
                                                    </div>
                                                    </>
                                                ) 
                                                :
                                                countTicketsByStatus.dispatched
                                            }
                                        </Card.Text>
                                    </Link>
                        </Card>
                        </Col>
                        <Col xs='6'>
                            <Card className='card-ticket card-definition'>
                                <div className='card-definition-text'>
                                Dispatched tickets are tickets that was directed to a wrong group by the user 
                                and had to be dispatched from an agent to SuperAgent. SA has to assign these 
                                tickets to the right group.
                                </div>
                            </Card>
                        </Col>
                        
                    </Row>
                    :
                    <></>
                    }
                    
                    <Row>
                        <Col xs='6'>
                            <Card className='card-ticket'>
                                <Card.Header>Ongoing Ticket</Card.Header>
                                <Link style={{textDecoration: 'none'}} >
                                    <Card.Text id='On Progress' onClick={(e) => handleClickFilterTicket(e)}>
                                        {isLoading ?
                                            (
                                                <>
                                                <div class="spinner-border card-loading-home-page" role="status">
                                                    <span class="visually-hidden">Loading...</span>
                                                </div>
                                                </>
                                            ) 
                                            :
                                            countTicketsByStatus['on progress']
                                        }
                                    </Card.Text>
                                </Link>
                            </Card>
                        </Col>
                        <Col xs='6'>
                            <Card className='card-ticket card-definition'>
                                <div className='card-definition-text'>
                                Ongoing tickets are tickets that are currently being handled by an agent, 
                                including on-progress tickets and solved tickets.
                                </div>
                            </Card>
                        </Col>
                    </Row>
                    
                    <Row>
                        <Col xs='6'>
                            <Card className='card-ticket'>
                                <Card.Header>Solved Ticket</Card.Header>
                                <Link style={{textDecoration: 'none'}} >
                                    <Card.Text id='Solved' onClick={(e) => handleClickFilterTicket(e)}>  
                                        {isLoading ?
                                            (
                                                <>
                                                <div class="spinner-border card-loading-home-page" role="status">
                                                    <span class="visually-hidden">Loading...</span>
                                                </div>
                                                </>
                                            ) 
                                            :
                                            countTicketsByStatus.solved
                                        }
                                    </Card.Text>
                                </Link>
                            </Card>
                        </Col>
                        <Col xs='6'>
                            <Card className='card-ticket card-definition'>
                                <div className='card-definition-text'>
                                Solved tickets are tickets that has been resolved by the agent but not yet closed by the user.
                                </div>
                            </Card>
                        </Col>
                    </Row>

                    <Row>
                        <Col xs='6'>
                            <Card className='card-ticket'>
                                <Card.Header>Closed Ticket</Card.Header>
                                <Link style={{textDecoration: 'none'}} >
                                    <Card.Text id='Closed' onClick={(e) => handleClickFilterTicket(e)}>  
                                        {isLoading ?
                                            (
                                                <>
                                                <div class="spinner-border card-loading-home-page" role="status">
                                                    <span class="visually-hidden">Loading...</span>
                                                </div>
                                                </>
                                            ) 
                                            :
                                            countTicketsByStatus.closed
                                        }
                                    </Card.Text>
                                </Link>
                            </Card>
                        </Col>
                        <Col xs='6'>
                            <Card className='card-ticket card-definition'>
                                <div className='card-definition-text'>
                                Closed tickets are tickets that has been solved for more that 48 hours or closed by the user, 
                                as well as tickets that have been reviewed by the user.
                                </div>
                            </Card>
                        </Col>
                    </Row>                    
                    </Col>
                
                    <Col className='col-img-body' xs='4' lg='7'>
                        <Row>
                            <h2 align='center'>WELCOME TO <img alt='logo' width='148px' src={logo} margin='auto'/></h2>
                            <h5 className='subtitle-home'>"Bridging question to solution"</h5>
                        </Row>
                        <Row>
                            <img alt='images' width='200%' src={images}/>
                        </Row>
                    </Col>
                    </Row>
                </Container>
            </body>
            <Footer/>
        </>
    )
}

export default Home;