//modules
import { useState, useEffect, useMemo } from "react";
import { get } from "lodash";
import { Link } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import DataTable from "react-data-table-component";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTrashAlt, faTimes, faPlusCircle } from "@fortawesome/free-solid-svg-icons";
import { Container, Row, Col, Form, Modal, Button } from "react-bootstrap";
//redux
import { addCategory, changeRoleApi, deleteCategory, deleteUserApi, fetchGroups, fetchRoles, fetchUsers } from "../redux/users/action";
//components
import Header from "../components/Header";
import Footer from "../components/Footer";
//styles & assets
import { manageUserTableStyles } from "../assets/js/tableStyles";
import '../assets/scss/manageUser.scss'

// pagination data table customize
const paginationRowsPerPageOptions = [5,10,15]

const ManageUser = () => {

    const dispatch = useDispatch()

    const token = useSelector((states) => states.auth.token)
    const groups = useSelector((states) => states.users.groups)
    const roles = useSelector((states) => states.users.roles)
    const users = useSelector((states) => get(states.users, 'usersData', []))
    const countUsers = useSelector(states => states.users.allUsersCount)

    const [filter, setFilter] = useState({})
    const [filteredUsers, setFilteredUsers] = useState([])
    const [isLoading, setIsLoading] = useState(false)
    const [isUserDataLoading, setIsUserDataLoading] = useState(false)
    const [showModal, setShowModal] = useState(false)
    const [currentUserGroups, setCurrentUserGroups] = useState('')
    const [showModalAssignGroup, setShowModalAssignGroup] = useState(false)
    const [showModalDeleteGroup, setShowModalDeleteGroup] = useState(false)
    const [showModalDeleteUser, setShowModalDeleteUser] = useState(false)
    const [showModalSuccessDeleteUser, setShowModalSuccessDeleteUser] = useState(false)
    const [showModalFailDeleteUser, setShowModalFailDeleteUser] = useState(false)
    const [selectedCategoryId, setSelectedCategoryId] = useState(0)
    const [categoryToDelete, setCategoryToDelete] = useState()
    const [userIdToAddCategory, setUserIdToAddCategory ] = useState()
    const [userToDeleteCategory, setUserToDeleteCategory ] = useState()
    const [userIdToDelete, setUserIdToDelete ] = useState()
    const [userToChangeRole, setUserToChangeRole] = useState()
    const [roleChangeInput, setRoleChangeInput] = useState()
    
    const handleCloseModal = () => {
        setShowModal(false)
        setShowModalAssignGroup(false)
        setShowModalDeleteGroup(false)
        setShowModalDeleteUser(false)
        setShowModalSuccessDeleteUser(false)
        setShowModalFailDeleteUser(false)
    }
                    
    const handleDeleteGroup = (e, user, usersCategory) => {
        e.preventDefault();
        setCategoryToDelete(usersCategory)
        setUserToDeleteCategory(user)
        setShowModalDeleteGroup(true)
        
    }

    const handleConfirmDeleteGroup = async (e) => {
        e.preventDefault();
        setIsLoading(true)
        await dispatch(deleteCategory({
            token,
            mainUserId: userToDeleteCategory.mainUserId,
            categoryId: categoryToDelete.categoryId,
        }))
        .then( async() => {
            setIsLoading(false)
            handleCloseModal()
            dispatch(fetchUsers({token}))
        })
        .catch(()=> {
            setIsLoading(false)
            handleCloseModal()
        })
    }

    const handleChangeRole = (e, user) => {
        setUserToChangeRole(user)
        setRoleChangeInput(e.target.value)
        setShowModal(true)
    }

    const handleConfirmChangeRole = async(e) => {
        e.preventDefault()
        setIsLoading(true)
        let category = []
        for (let userCategory of userToChangeRole.usersCategories) {
            category.push(userCategory.categoryId)
        }

        const userEdited = {
            ...userToChangeRole,
            category,
            role: roleChangeInput,
        }

        // delete forbidden properties for edit user
        delete userEdited.mainUserId
        delete userEdited.usersCategories
        delete userEdited.jobLocation
        delete userEdited.description
        delete userEdited.profilePictUrl

        dispatch(changeRoleApi({token, userData: userEdited}))
        .then(() => {
            setIsLoading(false)
            handleCloseModal()
            dispatch(fetchUsers({token}))
        })
        .catch(() => {
            setIsLoading(false)
            handleCloseModal()
        })

    }

    const handleChangeFilter = (e) => {
        setFilter({
            ...filter,
            [e.target.id]: e.target.value,
        })
    }

    const handleAddGroup = (e, user) => {
        e.preventDefault();
        setShowModalAssignGroup(true)
        setUserIdToAddCategory(user.mainUserId)
        let userGroup = [];
        for(let data of user.usersCategories) {
            userGroup.push(data.category.name)
        }
        setCurrentUserGroups(userGroup)
    }

    const handleSelectCategory = (e) => {
        setSelectedCategoryId(e.target.value)
    }

    const handleConfirmAddGroup = async (e) => {
        setIsLoading(true)
        dispatch(addCategory({
            token,
            mainUserId: userIdToAddCategory,
            categoryId: selectedCategoryId,
        }))
        .then(async () => {
            setIsLoading(false)
            handleCloseModal()
            dispatch(fetchUsers({token}))
        })
        .catch(() => {
            setIsLoading(false)
            handleCloseModal()
        })
    }

    const handleDeleteUser = (e, uid) => {
        e.preventDefault();
        setShowModalDeleteUser(true)
        setUserIdToDelete(uid)
    }

    const handleConfirmDeleteUser = async(e) => {
        e.preventDefault();
        setIsLoading(true)
        dispatch(deleteUserApi({token, uid: userIdToDelete}))
        .then(() => {
            setShowModalDeleteUser(false)
            setShowModalSuccessDeleteUser(true)
            dispatch(fetchUsers({token}))
            setIsLoading(false)
        })
        .catch(() => {
            setShowModalFailDeleteUser(true)
            setIsLoading(false)
        })
    }

    // table headers for data table component
    const tableHeader = [
        {
            name: 'User Identity',
            selector: row => row.name,
            format: row => {
                return (
                    <>
                        <div className='user-name'>{row.name}</div>
                        <div className='user-nik'>{row.employeeNumber}</div>
                        <div className='user-email'>{row.username}</div>
                    </>
                )
            },
            maxWidth: '25%',
            sortable: true,
            wrap: true,
        },
        {
            name: 'Division/Position',
            selector: row => row.division,
            format: row => {
                return (
                    <>
                        <div className='user-name'>{row.division}</div>
                        <div className='user-nik'>{row.position}</div>
                    </>
                )
            },
            sortable: true,
            maxWidth: '15%',
            wrap: true,
        },
        {
            name: 'Role',
            cell: row => {
                return (
                    <Form.Select value={row.role} className='btn-yellow select-role' onChange={(e) => handleChangeRole(e, row)}>
                        {roles.map((role,i) => (
                            <option key={i}>{role}</option>
                        ))}
                    </Form.Select>
                )
            },
            maxWidth: '13%',
            minWidth: '150px',
            style: {
                justifyContent: 'center'
            }
        },
        {
            name: 'Group(s)',
            cell: row => {
                return (
                    <>
                    {row.usersCategories.map((usersCategory,idxGroup) => {
                        return (
                            <div key={idxGroup} className='btn-groups'>
                                {usersCategory.category.name}
                                <button className='close-button' onClick={(e) => handleDeleteGroup(e, row, usersCategory)}>
                                    <FontAwesomeIcon icon={faTimes} size='xs'/>
                                </button>
                            </div>
                        )})
                    }
                    <button className='delete-icon' onClick={(e) => handleAddGroup(e, row)}>
                        <FontAwesomeIcon icon={faPlusCircle} />
                    </button>
                    </>
                )
            },
            style: {
                display: 'table-cell'
            },
            wrap: true,
        },
        {
            name: 'Delete',
            cell: row => {
                return (
                    <>
                        <button className='delete-icon' disabled={row.role.replace('-', ' ').toLowerCase() === 'super agent'} onClick={(e) => handleDeleteUser(e, row.mainUserId)}>
                            <FontAwesomeIcon icon={faTrashAlt} />
                        </button>
                    </>
                )
            },
            maxWidth: '5px',
            center: true
        }
    ]

    useMemo(() => {
        setFilteredUsers (
            users.filter((user, i) => {
                const userName = user.name.toLowerCase()
                let userGroups = []
                for (let category of user.usersCategories) {
                    userGroups.push(category.category.label)
                }
                const userRoles = user.role
                let nameFilterRes;
                try {
                    nameFilterRes = userName.includes(filter.name.toLowerCase())
                }
                catch {
                    nameFilterRes = true
                }
                const groupFilterRes = filter.group && filter.group !== 'All' ? userGroups.includes(filter.group) : true
                const roleFilterRes = filter.role && filter.role !== 'All' ? userRoles.replace('-', ' ').toLowerCase() === filter.role.replace('-', ' ').toLowerCase() : true
                return (
                    nameFilterRes && groupFilterRes && roleFilterRes
            )})
        )
    }, [filter.group, filter.name, filter.role, users])

    useEffect(() => {

        const fetchUsersData = () => {
            setIsUserDataLoading(true)
            dispatch(fetchUsers({token}))
            .then(() => {
                setIsUserDataLoading(false)
            })
        }

        const fetchRolesData = () => {
            dispatch(fetchRoles(token))
        }

        const fetchGroupsData = () => {
            dispatch(fetchGroups(token))
        }

        fetchUsersData()
        fetchRolesData()
        fetchGroupsData()

    }, [countUsers, dispatch, token])


    return (
        <>
            <header>
                <Header />
            </header>
            <div className='body-manage-user'>
                <Container>
                    <Row>
                        <div className='title h3'>Manage Users</div>
                    </Row>
                    <Row className='justify-content-center filter-root'>
                        <Col xs='6' lg='2' className='filter'>
                            <Row>
                                Nama
                            </Row>
                            <Row>
                                <Form.Control id='name' className='input-text' placeholder='enter user name' onChange={(e) => handleChangeFilter(e)}/>
                            </Row>
                        </Col>
                        <Col xs='6' lg='2' className='filter'>
                            <Row>Group</Row>
                            <Row>
                                <Form.Select id='group' className='input-select' onChange={(e) => handleChangeFilter(e)} >
                                    <option key='all'>All</option>
                                    {groups.map((group, i) => (
                                        <option key={i}>{group.category}</option>
                                    ))}
                                </Form.Select>
                            </Row>
                        </Col>
                        <Col xs='6' lg='2' className='filter'>
                            <Row>Role</Row>
                            <Row>
                                <Form.Select id='role' className='input-select' onChange={(e) => handleChangeFilter(e)}>
                                    <option key='all'>All</option>
                                    {roles.map((role, i) => (
                                        <option key={i}>{role}</option>
                                    ))}
                                </Form.Select>
                            </Row>
                        </Col>
                    </Row>
                    <Row className='add-user-row justify-content-end'>
                        <Col xs='8' lg='8'>
                            { }
                        </Col>
                        <Col xs='auto' lg='auto'>
                            <Link to='/new-user'>
                                <button className='ms-auto btn-add-user'>+ Add User</button>
                            </Link>
                        </Col>
                    </Row>
                    <Row>
                        {isUserDataLoading ? 
                            <div className='d-flex justify-content-center'>
                                <div className='spinner-border text-primary'>
                                    <span className='visually-hidden'></span>
                                </div>
                            </div>
                            : 
                            <div className='table-wrapper-manage-user'>
                                <DataTable 
                                    responsive
                                    fixedHeader
                                    fixedHeaderScrollHeight = '52vh'
                                    columns={tableHeader}
                                    data = {filteredUsers}
                                    customStyles = {manageUserTableStyles}
                                    highlightOnHover
                                    pagination
                                    paginationRowsPerPageOptions = {paginationRowsPerPageOptions}
                                    paginationPerPage = {5}
                                    paginationComponentOptions={{
                                        selectAllRowsItem: true,
                                    }}
                                />
                            </div>
                            }
                    </Row>
                </Container>
                <Modal id='modal-change-role' centered show={showModal} onHide={handleCloseModal} contentClassName='modal-paper'>
                    <Modal.Header className='modal-header-custom' closeButton />
                    <Modal.Body>
                        <p>You will make a change of role to this user.<br/> Continue?</p>
                    </Modal.Body>
                    <Modal.Footer className='modal-footer-custom'>
                        <Button size='sm' variant='orange' className='btn-modal' onClick={(e) => handleConfirmChangeRole(e)}>
                            {isLoading ?
                                (
                                    <>
                                    <span className='spinner-border spinner-border-sm'></span>
                                    {'  '}Loading...
                                    </>
                                ) :
                                "Confirm"
                            }
                        </Button>
                    </Modal.Footer>
                </Modal>
                <Modal id='modal-assign-group' centered show={showModalAssignGroup} onHide={handleCloseModal} contentClassName='modal-paper-yellow'>
                    <Modal.Header className='modal-header-custom' closeButton />
                    <Modal.Body className='modal-group-body'>
                            <p className='modal-group-title'>Assign to a Group</p>
                            <Form.Select defaultValue='Select' className='modal-group-select' onChange={(e) => handleSelectCategory(e)}>
                                <option key='disabled' disabled>Select</option>
                                {groups.map((group, i) => (
                                    currentUserGroups.includes(group.category) ? 
                                        (<></>)
                                        :
                                        (<option key={i} value={group.categoryId}>{group.category}</option>)
                                ))}
                            </Form.Select>
                    </Modal.Body>
                    <Modal.Footer className='modal-footer-custom'>
                        <Button disabled={selectedCategoryId === 0} size='sm' className='btn-modal' onClick={(e) => handleConfirmAddGroup(e)}>
                            {isLoading ?
                                (
                                    <>
                                    <span className='spinner-border spinner-border-sm'></span>
                                    {'  '}Loading...
                                    </>
                                ) :
                                "Assign"
                            }
                        </Button>
                    </Modal.Footer>
                </Modal>
                <Modal id='modal-delete-group' centered show={showModalDeleteGroup} onHide={handleCloseModal} contentClassName='modal-paper-yellow'>
                    <Modal.Header className='modal-header-custom' closeButton />
                    <Modal.Body className='modal-group-body'>
                    <p>Confirm delete group?</p>
                    </Modal.Body>
                    <Modal.Footer className='modal-footer-custom'>
                        <Button size='sm' className='btn-modal' onClick={(e) => handleConfirmDeleteGroup(e)}>
                        {isLoading ?
                            (
                                <>
                                <span className='spinner-border spinner-border-sm'></span>
                                {'  '}Loading...
                                </>
                            ) :
                            "Confirm"
                        }
                        </Button>
                    </Modal.Footer>
                </Modal>
                <Modal id='modal-delete-user' centered show={showModalDeleteUser} onHide={handleCloseModal} contentClassName='modal-paper-yellow'>
                    <Modal.Header className='modal-header-custom' closeButton />
                    <Modal.Body className='modal-group-body'>
                    <p>You will delete all data of this user. Continue?</p>
                    </Modal.Body>
                    <Modal.Footer className='modal-footer-custom'>
                        <Button size='sm' className='btn-modal' onClick={(e) => handleConfirmDeleteUser(e)}>
                        {isLoading ?
                            (
                                <>
                                <span className='spinner-border spinner-border-sm'></span>
                                {'  '}Loading...
                                </>
                            ) :
                            "Confirm"
                        }
                        </Button>
                    </Modal.Footer>
                </Modal>
                <Modal id='modal-sucess-delete-user' centered show={showModalSuccessDeleteUser} onHide={handleCloseModal} contentClassName='modal-paper-yellow'>
                    <Modal.Header className='modal-header-custom' closeButton />
                    <Modal.Body className='modal-group-body'>
                        <p>Delete user success!</p>
                    </Modal.Body>
                    <Modal.Footer className='modal-footer-custom'>
                        <Button size='sm' className='btn-modal' onClick={() => handleCloseModal()}>
                            Close
                        </Button>
                    </Modal.Footer>
                </Modal>
                <Modal id='modal-fail-delete-user' centered show={showModalFailDeleteUser} onHide={handleCloseModal} contentClassName='modal-paper-yellow'>
                    <Modal.Header className='modal-header-custom' closeButton />
                    <Modal.Body className='modal-group-body'>
                        <p>Delete user failed!</p>
                    </Modal.Body>
                    <Modal.Footer className='modal-footer-custom'>
                        <Button size='sm' className='btn-modal' onClick={() => handleCloseModal()}>
                            Close
                        </Button>
                    </Modal.Footer>
                </Modal>
            </div>
            <footer>
                <Footer />
            </footer>
        </>
    )
}

export default ManageUser;