//modules
import { Button, Container, Row, Form, Col, Alert } from "react-bootstrap"
import DataTable from "react-data-table-component";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPen, faTimes } from "@fortawesome/free-solid-svg-icons";
import { useEffect, useState } from "react"
import { useDispatch, useSelector } from "react-redux"
import { get } from "lodash";
//components
import Header from "../components/Header"
import ModalConfirmation from "../components/ModalConfirmation";
//redux
import {
    addCategoryApi,
    deleteCategoryApi,
    editCategoryApi,
    fetchCategoriesApi
} from "../redux/categories/action"
//styles & assets
import styles from "../assets/scss/manageCategories.module.scss"
import { manageCategoriesTableStyles } from "../assets/js/tableStyles";

const ManageCategories = () => {

    const dispatch = useDispatch()
    const token = useSelector(states => states.auth.token)
    const categories = useSelector(states => states.categories.categoriesData)

    const [ isLoading, setIsLoading ] = useState(false)
    const [ isDataLoading, setIsDataLoading ] = useState(false)
    const [ isEditExisting, setIsEditExisting ] = useState(false)
    const [ isError, setIsError ] = useState(false)
    const [ showModalEditCategory, setShowModalEditCategory ] = useState(false)
    const [ showModalConfirmDeleteCategory, setShowModalConfirmDeleteCategory ] = useState(false)
    const [ showAlert, setShowAlert ] = useState(false)
    const [ showAlertFetchData, setShowAlertFetchData ] = useState(false)
    const [ inputData, setInputData ] = useState({})
    const [ categoryToDelete, setCategoryToDelete ] = useState({})
    const [ alertMessage, setAlertMessage ] = useState('')
    const [ failFetchDataMessage, setFailFetchDataMessage ] = useState('')
    
    const handleCloseModal = () => {
        setShowModalEditCategory(false)
        setShowModalConfirmDeleteCategory(false)
        setIsEditExisting(false)
    }

    const handleSave = () => {

        const fetchCategories = () => {
            setIsDataLoading(true)
            dispatch(fetchCategoriesApi({token}))
            .then(res => {
                setIsDataLoading(false)
                setShowAlertFetchData(false)
            })
            .catch(err => {
                setIsDataLoading(false)
                setFailFetchDataMessage(err.message)
                setShowAlertFetchData(true)
            })
        }

        setIsLoading(true)
        if(isEditExisting) {
            delete inputData.createdAt
            delete inputData.updatedAt
            dispatch(editCategoryApi({token, data: inputData}))
            .then(res => {
                setIsLoading(false)
                setIsError(false)
                setShowModalEditCategory(false)
                fetchCategories()
                setShowAlert(true)
                setAlertMessage("Edit category success!")
            })
            .catch(err => {
                setIsLoading(false)
                setIsError(true)
                setShowModalEditCategory(false)
                setShowAlert(true)
                setAlertMessage("Edit category failed! " + err.message)
            })
        }
        else {
            dispatch(addCategoryApi({token, data: inputData}))
            .then(res => {
                setIsLoading(false)
                setIsError(false)
                setShowModalEditCategory(false)
                fetchCategories()
                setShowAlert(true)
                setAlertMessage("Add category success!")
            })
            .catch(err => {
                setIsLoading(false)
                setIsError(true)
                setShowModalEditCategory(false)
                setShowAlert(true)
                setAlertMessage("Add category failed! " + err.message)
            })
        }
    }

    const editCategory = (categoryToEdit) => {
        setInputData(categoryToEdit)
        setIsEditExisting(true)
        setShowModalEditCategory(true)
    }

    const handleAddCategory = () => {
        setInputData({})
        setShowModalEditCategory(true)
    }

    const handleDeleteCategory = (categoryData) => {
        setShowModalConfirmDeleteCategory(true)
        setCategoryToDelete(categoryData)
    }

    const handleConfirmDeleteCategory = () => {

        const fetchCategories = () => {
            setIsDataLoading(true)
            dispatch(fetchCategoriesApi({token}))
            .then(res => {
                setIsDataLoading(false)
                setShowAlertFetchData(false)
            })
            .catch(err => {
                setIsDataLoading(false)
                setFailFetchDataMessage(err.message)
                setShowAlertFetchData(true)
            })
        }

        setIsLoading(true)
        dispatch(deleteCategoryApi({token, categoryId: categoryToDelete.categoryId}))
        .then(res => {
            setIsLoading(false)
            setIsError(false)
            setShowModalConfirmDeleteCategory(false)
            fetchCategories()
            setShowAlert(true)
            setAlertMessage("Delete category success!")
        })
        .catch(err => {
            setIsLoading(false)
            setIsError(true)
            setShowModalConfirmDeleteCategory(false)
            setShowAlert(true)
            setAlertMessage("Delete category failed! " + err.message)
        })
    }

    const manageButton = (categoryData) => {
        return(
            <>
            <Button className={styles.buttonManage} onClick={() => editCategory(categoryData)}>
                <FontAwesomeIcon icon={faPen} size='xs'/>
            </Button>
            <Button variant='orange' className={styles.buttonManage} onClick={() => handleDeleteCategory(categoryData)}>
                <FontAwesomeIcon icon={faTimes} size='xs'/>
            </Button>
            </>
        )
    }

    useEffect(() => {

        const fetchCategories = () => {
            setIsDataLoading(true)
            dispatch(fetchCategoriesApi({token}))
            .then(res => {
                setIsDataLoading(false)
                setShowAlertFetchData(false)
            })
            .catch(err => {
                setIsDataLoading(false)
                setFailFetchDataMessage(err.message)
                setShowAlertFetchData(true)
            })
        }
        
        fetchCategories()

    }, [dispatch, token])

    const modalMessage =
        <Container>
            <div className={styles.modalTitle}>
                {isEditExisting ? 
                    "Edit Category"
                    :
                    "Add New Category"
                }
            </div>
            <Form.Label className={styles.formLabel}>Category</Form.Label>
            <Form.Control
                type='text'
                className={styles.formInput}
                placeholder='insert category name'
                id='label'
                defaultValue={get(inputData, 'label', '')}
                onChange={(e) => setInputData({
                    ...inputData,
                    label: e.target.value,
                    category: e.target.value
                })}
            />  
            <Form.Label className={styles.formLabel}>Description</Form.Label>
            <Form.Control
                as='textarea'
                className={styles.formInput}
                placeholder='insert description'
                defaultValue={get(inputData, 'description', null)}
                onChange={(e) => setInputData({
                    ...inputData,
                    description: e.target.value
                })}
                style={{ minHeight: '150px' }}
            />  
        </Container>

    const tableHeader = [
        {
            name: 'No',
            selector: (row, index) => index+1,
            sortable: true,
            maxWidth: '20px',
        },
        {
            name: 'Category',
            selector: row => row.category,
            sortable: true,
            maxWidth: '26%',
            wrap: true
        },
        {
            name: 'Description',
            selector: row => row.description,
            wrap: true,
            sortable: true,
        },
        {
            name: 'Manage',
            selector: row => manageButton(row),
            sortable: true,
            maxWidth: '150px',
            wrap: true,
        },
    ]
    
    return (
        <>
        <header>
            <Header />
        </header>
        <body className={styles.body}>
            <Container>
                <Alert show={showAlert} variant={isError? 'danger' : 'success'} id='fail-manage-category'>{alertMessage}</Alert>
                <Alert show={showAlertFetchData} variant='danger' id='fail-fetch-category'>{`Load categories data failed! ${failFetchDataMessage}`}</Alert>
                <Row>
                    <div className={`h3 ${styles.title}`}>Manage Categories</div>
                </Row>
                <Row className='add-user-row justify-content-end'>
                        <Col xs='8' lg='8'>
                            { }
                        </Col>
                        <Col xs='auto' lg='auto'>
                            <button className='ms-auto btn-add-user' onClick={() => handleAddCategory()}>+ Add Category</button>
                        </Col>
                    </Row>
                <Row>
                {isDataLoading ? 
                    <div className='d-flex justify-content-center'>
                        <div className='spinner-border text-primary'>
                            <span className='visually-hidden'></span>
                        </div>
                    </div>
                    :
                    <div className='table-wrapper-manage-user'>
                        <DataTable 
                            responsive
                            fixedHeader
                            fixedHeaderScrollHeight = '52vh'
                            columns={tableHeader}
                            data = {categories}
                            customStyles = {manageCategoriesTableStyles}
                            highlightOnHover
                            pagination
                            paginationComponentOptions={{
                                selectAllRowsItem: true,
                            }}
                        />
                    </div>
                }
                </Row>
            </Container>
            <ModalConfirmation 
                showModal={showModalEditCategory}
                handleCloseModal={()=>handleCloseModal()}
                message={modalMessage}
                handleConfirm={handleSave}
                isLoading={isLoading}
                saveButton='Save'
                cancelButton='Cancel'
            />
            <ModalConfirmation
                showModal={showModalConfirmDeleteCategory}
                handleCloseModal={()=>handleCloseModal()}
                message='You are about to delete this category. Continue?'
                handleConfirm={handleConfirmDeleteCategory}
                isLoading={isLoading}
                variant='Yellow'
            />
        </body>
        </>
    )
}

export default ManageCategories