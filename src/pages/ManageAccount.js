//modules
import { useState, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useForm } from "react-hook-form";
import get from 'lodash/get'
import 'external-svg-loader'
import { Col, Container, Form, InputGroup, Row, Button, Alert } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPencilAlt } from '@fortawesome/free-solid-svg-icons'
//redux
import { forgotPassApi, updateProfileApi } from '../redux/auth/action';
import { fetchProfile } from '../redux/auth/action';
//components
import ModalNotification from '../components/ModalNotification';
import ModalConfirmation from '../components/ModalConfirmation';
import Header from '../components/Header'
import Footer from '../components/Footer'
//styles and assets
import styles from '../assets/scss/manageAccount.module.scss'
import imgTriangle from '../assets/images/signin-left-triangle.png'
import CircleIllustration from '../assets/images/circle-illustration.svg'


const ManageAccount = () => {

    const dispatch = useDispatch()

    const [toggleEditName, setToggleEditName] = useState(false)
    const [toggleEditEmail, setToggleEditEmail] = useState(false)
    const [showModalNotification, setShowModalNotification] = useState(false)
    const [showModalConfirmChangePass, setShowModalConfirmChangePass] = useState(false)
    const [isLoading, setIsLoading] = useState(false)
    const [notificationMessage, setNotificationMessage] = useState('')
    const [needLogout, setNeedLogout] = useState(false)
    const [showAlert, setShowAlert] = useState(false)
    const [errorMessage, setErrorMessage] = useState('')

    const profile = useSelector(states => get(states.auth, 'profile', {}))
    const token = useSelector(states => get(states.auth, 'token', ''))
    const userId = useSelector(states => get(states.auth, 'userId', ''))

    const { register, handleSubmit, formState: {errors}, reset } = useForm({name: profile.name, username: profile.username})
    
    const handleLogout = () => {
        dispatch({type: "LOGOUT"})
    }

    const handleCloseModal = () => {
        setShowModalNotification(false)
        if (needLogout) handleLogout()
    }

    const handleCancel = () => {
        setToggleEditName(false)
        setToggleEditEmail(false)
        reset({name: profile.name, username: profile.username})
    }

    const handleSaveProfile = async (data) => {
        setIsLoading(true)

        let userData = {
            ...profile,
            ...data
        }
        //delete forbidden fields for update profile api
        delete userData.mainUserId
        delete userData.usersCategories
        delete userData.jobLocation
        delete userData.description
        delete userData.profilePictUrl

        dispatch(updateProfileApi({token, userId, userData}))
        .then(async () => {
            setNotificationMessage('Your profile has been successfully updated!')
            setNeedLogout(false)
            setShowModalNotification(true)
            //fetch profile after update
            dispatch(fetchProfile({userId, token}))
            .then(() => {
                setIsLoading(false)
            })
        })
        .catch(err => {
            setErrorMessage(err.message)
            setShowAlert(true)
            setIsLoading(false)
        })
    }

    const handleConfirmChangePassword = () => {
        setIsLoading(true)
        dispatch(forgotPassApi(profile.username))
        .then((res) => {
            setShowModalConfirmChangePass(false)
            setIsLoading(false)
            setNeedLogout(true) //logout after close modal
            setNotificationMessage('An e-mail has been sent to you with a link to reset your password. You will be logged out now.')
            setShowModalNotification(true)
        })
        .catch(err => {
            setErrorMessage(err.message)
            setShowAlert(true)
            setShowModalConfirmChangePass(false)
            setIsLoading(false)
        })
    }

    useEffect(() => {

        const handleCancel = () => {
            setToggleEditName(false)
            setToggleEditEmail(false)
            reset({name: profile.name, username: profile.username})
        }

        const fetchProfileFunc = () => {
            dispatch(fetchProfile({userId, token}))
        }
        
        handleCancel()
        fetchProfileFunc()

    }, [dispatch, profile.name, profile.username, reset, token, userId])

    return(
        <>
        <header>
            <Header />
        </header>
        <body className={styles.body}>
            <div className='hr-wrapper'>
                <hr/>
                <hr/>
                <hr/>
                <hr/>
                <hr/>
            </div>
            <svg 
                data-src={CircleIllustration}
                width='150px'
                className={styles.circleIllustration}
            />
            <img alt='illustration-triangle' src={imgTriangle} className='img-triangle' height='400px' />
            <Alert show={showAlert} variant='danger' id='fail-get-ticket'>{errorMessage}</Alert>
            <div>
                <Container>
                    <Row className={styles.formRoot}>
                        <Col className={styles.blueAccent}>
                        </Col>
                        <Col className={styles.formContent}>
                            <div className={`h4 ${styles.formTitle}`}>Manage Account</div>
                            <Form onSubmit={(e) => e.preventDefault()}>
                                <Form.Group className={styles.inputWrapper}>
                                    <Form.Label className={styles.formLabel}>
                                        Nama
                                    </Form.Label>
                                    <InputGroup>
                                        <Form.Control
                                            id='name'
                                            type='text'
                                            plaintext={!toggleEditName}
                                            readOnly={!toggleEditName}
                                            defaultValue={profile.name}
                                            className={toggleEditName ? styles.formInput : styles.formInputDisable }
                                            {...register('name', {
                                                required: {
                                                    value: true,
                                                    message: 'You must specify a name'
                                                }
                                            })}
                                        />
                                        {!toggleEditName ? 
                                            <InputGroup.Text className={styles.toggleInputWrapper}>
                                                    <Link onClick={() => setToggleEditName(!toggleEditName)} className={styles.toggleInput}>
                                                        <FontAwesomeIcon icon={faPencilAlt} />change name
                                                    </Link>
                                            </InputGroup.Text>
                                            :
                                            <></>
                                        }
                                    </InputGroup>
                                    {!!errors.name && <div className='form-text text-danger'>{errors.name.message}</div>}
                                </Form.Group>
                                <Form.Group className={styles.inputWrapper}>
                                    <Form.Label className={styles.formLabel}>
                                        Email
                                    </Form.Label>
                                    <div className={styles.formInputDisable}>{profile.username}</div>
                                    
                                </Form.Group>
                                <div className={styles.inputWrapper}>
                                    <p className={styles.formLabel}>
                                        NIK
                                    </p>
                                    <div className={styles.formInputDisable}>{profile.employeeNumber}</div>
                                </div>
                                <div className={styles.inputWrapper}>
                                    <p className={styles.formLabel}>
                                        Role
                                    </p>
                                    <div className={styles.formInputDisable}>{profile.role}</div>
                                </div>
                                <div className={styles.inputWrapper}>
                                    <p className={styles.formLabel}>
                                        Divisi
                                    </p>
                                    <div className={styles.formInputDisable}>{profile.division}</div>
                                </div>
                                <div className={styles.inputWrapper}>
                                    <p className={styles.formLabel}>
                                        Posisi
                                    </p>
                                    <div className={styles.formInputDisable}>{profile.position}</div>
                                </div>
                                <div className={styles.inputWrapper}>
                                    <p className={styles.formLabel}>
                                        Group
                                    </p>
                                    <div>
                                        {get(profile, 'usersCategories', []).map((usersCategory,i) => {
                                            return (
                                                <>
                                                    <button disabled className={styles.btnGroups}>
                                                        {usersCategory.category.name}
                                                    </button>
                                                </>
                                                )
                                            }
                                        )}
                                    </div>
                                </div>
                                <div className={`${styles.inputWrapper} d-flex justify-content-between`}>
                                    <div>
                                        <p className={styles.formLabel}>
                                            Password
                                        </p>
                                        <div className={styles.formInputDisable}>******</div>
                                    </div>
                                    <div className='d-flex align-items-end'>
                                        <Button
                                            className={styles.btnChangePass}
                                            onClick={() => setShowModalConfirmChangePass(true)}
                                        >
                                            change password
                                        </Button>
                                    </div>
                                </div>
                            {toggleEditName || toggleEditEmail ? 
                                <div className={`${styles.btnWrapper}`}>
                                    <Button variant='black' className={styles.btnCancel} onClick={() => handleCancel()}>Cancel</Button>
                                    <Button variant='orange' className={styles.btnSave} onClick={handleSubmit(handleSaveProfile)}>
                                        {isLoading ?
                                            (
                                                <>
                                                <span className='spinner-border spinner-border-sm'></span>
                                                {'  '}Loading...
                                                </>
                                            ) :
                                            "Save"
                                        }
                                    </Button>
                                </div>
                                :
                                <></>
                            }
                            </Form>
                        </Col>
                    </Row>
                </Container>
            </div>
            <ModalNotification 
                showModal={showModalNotification}
                handleCloseModal={() => handleCloseModal()}
                message={notificationMessage}
            />
            <ModalConfirmation 
                showModal={showModalConfirmChangePass}
                handleCloseModal={()=>setShowModalConfirmChangePass(false)}
                message='We will send an email with a link to change your password. Continue? (You will be logged out)'
                handleConfirm={handleConfirmChangePassword}
                isLoading={isLoading}
            />
        </body>
        <Footer />
        </>
    )
}

export default ManageAccount