//modules
import { useState } from "react";
import { Col, Container, Form, Row, Button, Alert, InputGroup } from "react-bootstrap";
import { Link, useHistory, useLocation } from "react-router-dom";
import { useDispatch } from "react-redux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEye, faEyeSlash } from "@fortawesome/free-solid-svg-icons";
//redux
import { loginAPI } from "../redux/auth/action";
//styles & assets
import '../assets/scss/login.scss'
import logo from '../assets/logo/logo-solver.png'
import imgSignin from '../assets/images/signin.png'
import imgTriangle from '../assets/images/signin-left-triangle.png'
import CircleIllustration from '../assets/images/circle-illustration-login.svg'


const Login = () => {
    const dispatch = useDispatch()
    const history = useHistory()
    const location = useLocation()
    const { from } = location.state || {from: {pathname: '/'}} //redirect to protected page after login

    const [inputData, setInputData] = useState({})
    const [isLoading, setIsLoading] = useState(false)
    const [showAlertLogin, setShowAlertLogin] = useState(false)
    const [errorMessage, setErrorMessage] = useState('')
    const [toggleShowPassword, setToggleShowPassword] = useState(false)

    const handleChangeInput = (e) => {
        setInputData({
            ...inputData,
            [e.target.id]: e.target.value
        })
    }
    
    const handleLogin = async () => {
        setIsLoading(true)
        dispatch(loginAPI(inputData))
        .then(data => {
            setIsLoading(false)
            if(data && data.token && data.role !== 'User')  {
                history.push(from)
            } else if(data.role === 'User') {
                setErrorMessage('You are not an agent!')
                setShowAlertLogin(true)                
            } else if(data.code === 'error') {
                setErrorMessage(data.message)
                setShowAlertLogin(true)
            }
        })
        .catch(err => {
            setErrorMessage(err)
            setShowAlertLogin(true)
            setIsLoading(false)
        })
    }


    return(
        <>
        <div className='logo-wrapper-login'>
            <img alt='logo' src={logo} className='img-logo' width='200'/>
        </div>
        <div className='hr-wrapper'>
            <hr/>
            <hr/>
            <hr/>
            <hr/>
            <hr/>
        </div>
        <svg 
            data-src={CircleIllustration}
            width='120px'
            className={'circleIllustration-login'}
        />
        <img alt='illustration-triangle' src={imgTriangle} className='img-triangle' height='400px' />
        <div className='login-body'>
            <Container>
                <Row className='form-root'>
                <Alert variant='danger' show={showAlertLogin} width='auto'>{errorMessage}</Alert>
                    <Col xs={6} className='form-img'>
                        <img alt='illustration' src={imgSignin} width='120%' className='img-illustration'/>
                    </Col>
                    <Col xs={6} className='form-paper'>
                        <Container className='form-wrapper'>
                            <div className='h4 form-title'>User Login</div>
                            <Form>
                                <Form.Label className='label-login'>
                                    Username / Email
                                </Form.Label>
                                <Form.Control id='email' type='email' placeholder='example: john.smith@telkom.co.id' className='form-input' onChange={(e) => handleChangeInput(e)} />
                                <InputGroup>
                                </InputGroup>
                                <Form.Label className='label-login'>
                                    Password
                                </Form.Label>
                                <div className='form-input-wrapper'>
                                    <Form.Control className='form-input' id='password' type={toggleShowPassword ? 'text' : 'password'} placeholder='enter your password' onChange={(e) => handleChangeInput(e)}  />
                                    <Link className='toggle-password-login' onClick={() => setToggleShowPassword(!toggleShowPassword)}><FontAwesomeIcon icon={toggleShowPassword ? faEyeSlash : faEye} /></Link>
                                </div>
                                <div className='button-wrapper-login'>
                                    <Button variant='orange' className='btn-login' onClick={handleLogin}>
                                        {isLoading ?
                                            (
                                                <>
                                                <span className='spinner-border spinner-border-sm'></span>
                                                {'  '}Loading...
                                                </>
                                            ) :
                                            "Login"}
                                    </Button>
                                </div>
                                <div>
                                    <Link to='/forgot-password' className='reset-password-link'>Forgot password?</Link>
                                </div>
                            </Form>
                        </Container>
                    </Col>
                </Row>
            </Container>
        </div>
        </>
    )
}

export default Login;