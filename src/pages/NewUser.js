// modules
import { useState } from "react";
import { Col, Container, Form, Row, Button, Modal, Alert } from "react-bootstrap";
import { Link } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlusCircle, faTimes, faEye, faEyeSlash } from "@fortawesome/free-solid-svg-icons";
import { useForm } from "react-hook-form";
//redux
import { addUserApi } from "../redux/users/action";
//components
import Header from "../components/Header";
import Footer from "../components/Footer";
import ModalNotification from "../components/ModalNotification";
//styles & assets
import '../assets/scss/newUser.scss'

const groupObjectToArray = (objGroups) => {
    let arrGroup = []
    for (let objGroup of objGroups) {
        arrGroup.push(objGroup.category)
    }
    return arrGroup;
}

const NewUser = () => {
    const dispatch = useDispatch()
    
    const groups = useSelector((states) => states.users.groups)
    const roles = useSelector((states) => states.users.roles)
    const token = useSelector(states => states.auth.token)

    const [showModal, setShowModal] = useState(false)
    const [selectedCategory, setSelectedCategory] = useState()
    const [isLoading, setIsLoading] = useState(false)
    const [addGroup, setAddGroup] = useState([])
    const [showFailedAlert, setShowFailedAlert] = useState(false)
    const [showSuccessAlert, setShowSuccessAlert] = useState(false)
    const [errorMessage, setErrorMessage] = useState('')
    const [showPassword, setShowPassword] = useState(false)
    
    const { register, handleSubmit, formState: {errors}, reset} = useForm()

    const handleCloseModal = () => {
        setShowModal(false)
        setSelectedCategory(undefined)
    }

    const handleAddGroup = (e) => {
        e.preventDefault()
        setShowModal(true)
    }

    const handleDeleteGroup = (e, idxGroup) => {
        e.preventDefault();
        let tempAddGroup = [...addGroup]
        tempAddGroup.splice(idxGroup, 1)
        setAddGroup(tempAddGroup)
    }

    const handleConfirmAddGroup = (e) => {
        e.preventDefault()
        setAddGroup([
            ...addGroup,
            selectedCategory,
        ])
        handleCloseModal()
    }

    const handleSelectCategory = (e) => {
        setSelectedCategory(JSON.parse(e.target.value))
    }

    const handleCreateNewUser = async (data) => {
        setShowSuccessAlert(false)
        setIsLoading(true)
        let addGroupCategories = []
        for (let group of addGroup) {
            addGroupCategories.push(group.categoryId)
        }
        await dispatch(addUserApi({
            token,
            userData: {
                ...data,
                "statusActive": "ACTIVE",
                "statusBlocked": "ACTIVE",
                category: addGroupCategories
            }}))
        .then((res) => {
            if(res.data.code !== 'error') {
                setShowSuccessAlert(true)
                setIsLoading(false)
                reset({})
            } else {
                setErrorMessage(res.data.message)
                setShowFailedAlert(true)
                setIsLoading(false)
            }
        })
        .catch(err => {
            setErrorMessage(err.message)
            setShowFailedAlert(true)
            setIsLoading(false)
        })
    }

    return (
        <>
            <header>
                <Header />
            </header>
            <body className='body'>
                <Container className='root'>
                <Alert id='failed-alert' variant='danger' show={showFailedAlert}>
                    {errorMessage}
                </Alert>
                    <Row>
                        <div className='h3 title'>Create New User</div>
                    </Row>
                    <Row className='justify-content-center form-wrapper-new-user'>
                        <div className='paper'>
                            <Form onSubmit={(e) => e.preventDefault()}>
                                <Container className='form-new-user-root'>
                                    <Row>
                                        <Col sm='6'>
                                            <Form.Label>Nama</Form.Label>
                                            <Form.Control
                                                id='name'
                                                className='form-input'
                                                size='sm'
                                                placeholder='enter your full name'
                                                {...register('name', {
                                                    required: 'You must specify a name',
                                                })}
                                            />
                                            {!!errors.name && <div className='form-text error-message-new-user'>{errors.name.message}</div>}
                                        </Col>
                                        <Col sm='6'>
                                            <Form.Label>E-mail</Form.Label>
                                            <Form.Control
                                                id='username'
                                                type='email'
                                                className='form-input'
                                                size='sm'
                                                placeholder='example: john.smith@telkom.co.id'
                                                {...register('username', {
                                                    required: 'You must specify an email',
                                                    pattern: {
                                                        value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
                                                        message: "Enter a valid e-mail address",
                                                    }
                                                })}
                                            />
                                            {!!errors.username && <div className='form-text error-message-new-user'>{errors.username.message}</div>}
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col sm='6'>
                                            <Form.Label>Password</Form.Label>
                                            <div className='form-input-wrapper'>
                                                <Form.Control
                                                    id='password'
                                                    type={showPassword ? 'text' : 'password'}
                                                    className='form-input'
                                                    size='sm'
                                                    placeholder='enter your password'
                                                    {...register('password', {
                                                        required: 'You must specify a password',
                                                        minLength: {
                                                            value: 5,
                                                            message: 'Password must have at least 5 characters'
                                                        },
                                                        maxLength: {
                                                            value: 30,
                                                            message: 'Password must less than 30 characters'
                                                        },
                                                        pattern : {
                                                            value: /(?=.*\d)(?=.*[a-z])(?=.*[A-Z])/,
                                                            message: 'Password must contain at least a lower case letter, an upper case letter, and a numeric'
                                                        }
                                                    })}
                                                />
                                                <Link className='toggle-password-login' onClick={() => setShowPassword(!showPassword)}><FontAwesomeIcon icon={showPassword ? faEyeSlash : faEye} /></Link>
                                            </div>
                                            {!!errors.password && <div className='form-text error-message-new-user'>{errors.password.message}</div>}
                                        </Col>
                                        <Col sm='6'>
                                            <Form.Label>Phone Number</Form.Label>
                                            <Form.Control
                                                id='phoneNumber'
                                                className='form-input'
                                                size='sm'
                                                placeholder='insert phone number'
                                                {...register('phoneNumber', {
                                                    required: 'You must specify a phone number',
                                                    pattern: {
                                                        value: /^[0-9]*$/,
                                                        message: 'Phone number must contain only numbers'
                                                    }
                                                })}
                                            />
                                            {!!errors.phoneNumber && <div className='form-text error-message-new-user'>{errors.phoneNumber.message}</div>}
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col sm='6'>
                                            <Form.Label>NIK</Form.Label>
                                            <Form.Control
                                                id='employeeNumber'
                                                className='form-input'
                                                size='sm'
                                                placeholder='example: 940001'
                                                {...register('employeeNumber', {
                                                    required: 'You must specify a NIK',
                                                })}
                                            />
                                            {!!errors.employeeNumber && <div className='form-text error-message-new-user'>{errors.employeeNumber.message}</div>}
                                        </Col>
                                        <Col sm='6'>
                                            <Form.Label>Role</Form.Label>
                                            <Form.Select
                                                id='role'
                                                className='form-input'
                                                size='sm'
                                                {...register('role', {
                                                    required: 'You must specify a role',
                                                })}
                                            >
                                                <option selected disabled value="">Select</option>
                                                {roles.map((role, i) => (
                                                    <option key={i}>{role}</option>
                                                ))}
                                            </Form.Select>
                                            {!!errors.role && <div className='form-text error-message-new-user'>{errors.role.message}</div>}
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col sm='6'>
                                            <Form.Label>Divisi</Form.Label>
                                            <Form.Control 
                                                id='division'
                                                className='form-input'
                                                size='sm'
                                                placeholder='example: Div. Digital & Next Business'
                                                {...register('division', {
                                                    required: 'You must specify a division',
                                                })}
                                            />
                                            {!!errors.division && <div className='form-text error-message-new-user'>{errors.division.message}</div>}
                                        </Col>
                                        <Col sm='6'>
                                            <Form.Label>Posisi</Form.Label>
                                            <Form.Control
                                                id='position'
                                                className='form-input'
                                                size='sm'
                                                placeholder='example: Junior Designer'
                                                {...register('position', {
                                                    required: 'You must specify a position',
                                                })}
                                            />
                                            {!!errors.position && <div className='form-text error-message-new-user'>{errors.position.message}</div>}
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col sm='12'>
                                            <Form.Label>Group</Form.Label>
                                            <div className='form-roles'>
                                                {addGroup.map((group, idxGroup) => (
                                                    <button disabled className='btn-groups'>
                                                        {group.label}
                                                        <Link className='close-button' onClick={(e) => handleDeleteGroup(e, idxGroup)}>
                                                            <FontAwesomeIcon icon={faTimes} size='xs'/>
                                                        </Link>
                                                    </button>
                                                ))}
                                                <button className='plus-button' onClick={(e) => handleAddGroup(e)}><FontAwesomeIcon icon={faPlusCircle} /></button>
                                            </div>
                                        </Col>
                                    </Row>
                                    <Row className='justify-content-center'>
                                        {/* <Col> */}
                                            <Button type='submit' className='btn-create' onClick={handleSubmit(handleCreateNewUser)}>
                                                {isLoading ?
                                                (
                                                    <>
                                                    <span className='spinner-border spinner-border-sm'></span>
                                                    {'  '}Loading...
                                                    </>
                                                ) :
                                                "Create"}
                                            </Button>
                                        {/* </Col> */}
                                    </Row>
                                </Container>
                            </Form>
                        </div>
                    </Row>
                </Container>
                <Modal id='modal-assign-group' centered show={showModal} onHide={handleCloseModal} contentClassName='modal-paper-yellow'>
                    <Modal.Header className='modal-header-custom' closeButton />
                    <Modal.Body className='modal-group-body'>
                            <p className='modal-group-title'>Assign to a Group</p>
                            <Form.Select defaultValue='Select' className='modal-group-select' onChange={(e) => handleSelectCategory(e)}>
                                <option disabled>Select</option>
                                {groups.map((group, i) => (
                                    groupObjectToArray(addGroup).includes(group.category) ?
                                        <></>
                                        :
                                        <option value={JSON.stringify(group)}>{group.category}</option>
                                ))}
                            </Form.Select>
                    </Modal.Body>
                    <Modal.Footer className='modal-footer-custom'>
                        <Button disabled={selectedCategory === undefined} size='sm' className='btn-modal' onClick={(e) => handleConfirmAddGroup(e)}>
                            Assign
                        </Button>
                    </Modal.Footer>
                </Modal>
                <ModalNotification 
                    showModal={showSuccessAlert}
                    handleCloseModal={()=>setShowSuccessAlert(false)}
                    message='User has been added!'
                />
            </body>
            <Footer />
        </>
    )
}

export default NewUser;