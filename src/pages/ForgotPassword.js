//modules
import { useState } from "react";
import { Col, Container, Form, Row, Button, Modal, Alert } from "react-bootstrap";
import { useDispatch } from "react-redux";
//redux
import { forgotPassApi } from "../redux/auth/action";
//styles & assets
import '../assets/scss/login.scss'
import styles from '../assets/scss/forgotPassword.module.scss'
import logo from '../assets/logo/logo-solver.png'
import imgTriangle from '../assets/images/signin-left-triangle.png'
import CircleIllustration from '../assets/images/circle-illustration-login.svg'

const ForgotPassword = () => {
    const dispatch = useDispatch()

    const [show, setShow] = useState(false);
    const [showAlert, setShowAlert] = useState(false)
    const [username, setUsername] = useState('')
    const [errorMessage, setErrorMessage] = useState('')

    const handleClose = () => setShow(false);

    const handleSendButton = async() => {
        dispatch(forgotPassApi(username))
        .then(() => {
            setShow(true)
        })
        .catch(err => {
            setErrorMessage(err.message)
            setShowAlert(true)
        })
        
    }

    const handleInputEmail = (e) => {
        setUsername(e.target.value)
    }
    
    return (
        <>
        <div className='logo-wrapper-login'>
            <img alt='logo' src={logo} className='img-logo' width='200'/>
        </div>
        <div className='hr-wrapper'>
            <hr/>
            <hr/>
            <hr/>
            <hr/>
            <hr/>
        </div>
        <svg 
            data-src={CircleIllustration}
            width='120px'
            className={'circleIllustration-login'}
        />
        <img alt='illustration-triangle' src={imgTriangle} className='img-triangle' height='400px' />
        <div className='login-body'>
            <Container>
                <Alert show={showAlert} variant='danger'>
                    {errorMessage}
                </Alert>
                <Row className='form-root'>
                    <Col xs={6} className={styles.formImg}></Col>
                    <Col xs={6} className={styles.formPaper}>
                        <Container className='form-wrapper'>
                            <div className={`h4 ${styles.formTitle}`}>Forgot Password</div>
                            <Form>
                                {/* <Row> */}
                                    <Form.Label className='label-login'>
                                        Email
                                    </Form.Label>
                                    <Form.Control type='email' placeholder='example: john.smith@telkom.co.id' className='form-input-forgot-password' onChange={(e) => handleInputEmail(e)} />
                                {/* </Row> */}
                                <Row>
                                    <Col sm={12}>
                                        <p className={`fst-italic ${styles.textForgotPass}`}>We will send you a password reset link to your email</p>
                                    </Col>
                                    <Col sm={12}>
                                        <div className={styles.buttonWrapper}>
                                            <Button onClick={handleSendButton} size='sm' variant='orange' className='btn-login'>
                                                Send
                                            </Button>
                                        </div>
                                    </Col>
                                </Row>
                            </Form>
                        </Container>
                    </Col>
                </Row>
            </Container>

            
                <Modal show={show} onHide={handleClose} contentClassName={styles.modalCard} centered>
                    <Modal.Body className={styles.modalMessage}>An e-mail has been sent to you with a link to reset your password</Modal.Body>
                    <Modal.Footer className={styles.modalButtonFooter}> 
                        <Button variant="primary" onClick={handleClose} className={styles.modalButtonOk} >
                        OK
                        </Button>
                    </Modal.Footer>
                </Modal>

        </div>
        </>
    )
}

export default ForgotPassword;