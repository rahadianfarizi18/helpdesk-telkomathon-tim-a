//modules
import { useEffect, useState } from "react";
import numeral from 'numeral'
import { useIdleTimer } from "react-idle-timer";
import { useSelector, useDispatch } from "react-redux";
import { Container, Row, Col, Card, Form, Button, Modal, Tooltip, OverlayTrigger, Alert } from "react-bootstrap";
import { useLocation } from 'react-router-dom'
import { Link } from 'react-router-dom'
import lowerCase from 'lodash/lowerCase'
import get from 'lodash/get'
import isDate from 'lodash/isDate'
import defaultTo from 'lodash/defaultTo'
import isEmpty from "lodash/isEmpty";
import truncate from "lodash/truncate";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faStar as starSol } from "@fortawesome/free-solid-svg-icons";
import { faStar as starReg } from "@fortawesome/free-regular-svg-icons";

//redux
import {
    fetchDetailTicketApi,
    fetchStatusTicketRefApi,
    fetchTicketActivityApi,
    fetchTicketReviewApi,
    updateTicketApi
} from "../redux/tickets/action";

//components
import Header from "../components/Header";
import Footer from '../components/Footer';

//styles & assets
import '../assets/scss/resolveTicket.scss'

const ResolveTicket = () => {
    const dispatch = useDispatch()
    const location = useLocation()
    const mainTicketId = new URLSearchParams(location.search).get('ticketId') || get(location.state, 'mainTicketId', null)
    const dataTicket = useSelector(states => get(states.tickets, 'currentTicket', []))
    const groups = useSelector(states => states.users.groups)
    const status = useSelector(states => states.tickets.stats)
    const { role,token } = useSelector(states => states.auth)
    const ticketActivities = useSelector(states => get(states.tickets, 'ticketActivity', []))
    const ticketReview = useSelector(states => get(states.tickets, 'ticketReview', {rating: 0}))

    const [inputData, setInputData] = useState({})
    const [isLoading, setIsLoading] = useState(false)
    const [showModal, setShowModal] = useState(false)
    const [errorMessage, setErrorMessage] = useState(false)
    const [showAlert, setShowAlert] = useState(false)
    const [count, setCount] = useState(0)
    const [validated, setValidated] = useState(false)

    const handleCloseModal = () => {
        setShowModal(false)
    }

    const handleCancel = () => {
        setInputData({})
        setValidated(false)
    }

    const handleSaveData = (e) => {

        const inputDataToSend = {
            ...inputData,
            mainTicketId,
            comment: defaultTo(inputData.response,'')
        }

        const form = e.currentTarget;
        if (form.checkValidity() === false) {
            e.preventDefault();
            e.stopPropagation();
            setValidated(true)
        } else {
            e.preventDefault();
            setIsLoading(true)
            dispatch(updateTicketApi({token, data: inputDataToSend}))
            .then((res) => {
                dispatch(fetchDetailTicketApi({token, mainTicketId}))
                .then(() => {
                    setShowModal(true)
                    setInputData({})
                    setIsLoading(false)
                })
                .catch(err => {
                    setErrorMessage(err.message)
                    setShowAlert(true)
                    setIsLoading(false)
                })
            })
            .catch(err => {
                setErrorMessage(err.message)
                setIsLoading(false)
            })
            setValidated(false)
        }

    }

    const handleInput = (e) => {
        if(role.replace('-', ' ').toLowerCase() !== 'super agent' && e.target.id === 'group') {
            e.target.value = dataTicket.category
        }
        setInputData({
            ...inputData,
            [e.target.id]: e.target.value
        })
    }

    const dateTimeFormatter = (dateData) => {
        if (isDate(dateData)) {
            const date = dateData.getDate()
            const month = dateData.getMonth()
            const year = dateData.getFullYear().toString().substr(2,2)
            const hour = dateData.getHours()
            const minute = dateData.getMinutes()
            return (`${date}/${numeral(month+1).format('00')}/${year} ${hour}:${numeral(minute).format('00')}`)
        }

    }

    const starReview = (rating) => (
        <>
        {[...Array(rating)].map((value, i) => (
            <FontAwesomeIcon icon={starSol} />
        ))} 
        {[...Array(5-rating)].map((value, i) => (
            <FontAwesomeIcon icon={starReg} />
        ))}
        </>
    )

    useEffect(() => {
        
        const fetchDetailTicket = async () => {
            await dispatch(fetchDetailTicketApi({token, mainTicketId}))
            .catch(err => {
                setErrorMessage(err)
            })
        }
        
        const fetchStatusTicket = async () => {
            await dispatch(fetchStatusTicketRefApi(token))
            .catch(err => {
                setErrorMessage(err)
            })
        }

        const fetchTicketReview = async () => {
            await dispatch(fetchTicketReviewApi({token, ticketId: mainTicketId}))
            .catch(error => {
                setErrorMessage(error.message)
            })
        }

        fetchStatusTicket()
        fetchDetailTicket()
        fetchTicketReview()
        
    }, [dispatch, mainTicketId, token])


    //function to call ticket activity api when idle for 500ms
    const handleOnActive = () => {
        setCount((val) => val + 1);
      };

    useIdleTimer({
        timeout: 500,
        onActive: handleOnActive,
      });

    useEffect(() => {

        const fetchTicketActivity = () => {
            dispatch(fetchTicketActivityApi({token,ticketId:mainTicketId}))
        }

        fetchTicketActivity()
    }, [dispatch, mainTicketId, token, count])
    
    const viewTooltip = (props) => (
        props.message.length > props.maxLength ? 
            <Tooltip id='status-tooltip' {...props}>
                {props.message}
            </Tooltip>
            :
            <></>
    )

    return(
        <>
        <header>
            <Header />
            </header>
            <body className='resolve-ticket-body'>
                <Alert show={showAlert} variant='danger' id='fail-get-ticket'>{errorMessage}</Alert>
                <Container>
                    <Row>
                        <h3> 
                            <span className='fw-bold'>{'Manage Ticket > '}</span>
                            <span className='p-tittleManage'>{!!dataTicket ? dataTicket.mainTicketId : ''}</span>
                        </h3>

                        <Link to='/manage-tickets' exact className='back-link-resolve-ticket'>
                            {'< Back'}
                        </Link>
                    </Row>
                    <Row className='card-status-resolve-ticket-wrapper'>
                        <Card className='card-status'>
                            <Container className='status-wrapper-resolve-ticket'>
                                <Row>
                                    <Col className='d-flex align-items-center'>
                                        <div className='h3 tittle-status'>{'Ticket Status: '}
                                            <span className='status-ticket'>{dataTicket.statusTicket}</span>
                                        </div>
                                    </Col>
                                    <Col className='tittle-review'>
                                            <Row>
                                                <div>
                                                    {starReview(get(ticketReview,'rating',0))}
                                                </div>
                                            </Row>
                                            <Row><span>{'Review'}</span></Row>
                                            {/* <Row><span className='review-user'>{get(ticketReview, 'review', 'N/A')}</span></Row> */}
                                            <Row>
                                                <div className='review-wrapper-resolve-ticket'>
                                                    <OverlayTrigger
                                                        placement='bottom'
                                                        overlay={viewTooltip({message: get(ticketReview, 'review', 'N/A'), maxLength: 30})}
                                                    >
                                                        <span className='review-user'>
                                                            {truncate(get(ticketReview, 'review', 'N/A'), {
                                                                length: 30
                                                            })}
                                                        </span>
                                                    </OverlayTrigger>
                                                </div>
                                            </Row>
                                    </Col>
                                </Row>
                            </Container>
                        </Card>
                    </Row>   
                    <Row>
                        <Col lg='6' xl='5'>
                            <Row className='card-resolve-ticket-wrapper'>
                                <Card className='card-view'>
                                    <Row className='ticket-id'>
                                        <Col lg='3' xs='3'>
                                            <div>
                                                Ticket ID
                                            </div>
                                        </Col>
                                        <Col lg='1' xs='1'>
                                            :
                                        </Col>
                                        <Col lg='auto' xs='6'>
                                            <div >
                                                {!!dataTicket ? dataTicket.mainTicketId : ''}
                                            </div>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col lg='3' xs='3'>
                                            <Form.Label className='form-left'>
                                                Group
                                            </Form.Label>
                                        </Col>
                                        <Col lg='1' xs='1' className='form-left'>
                                            :
                                        </Col>
                                        <Col>
                                            <Form.Label className='form-right'>
                                                {!!dataTicket.category? dataTicket.category : ''}
                                            </Form.Label>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col lg='3' xs='3'>
                                            <Form.Label className='form-left'>
                                                Problem
                                            </Form.Label>
                                        </Col>
                                        <Col lg='1' xs='1' className='form-left'>
                                            :
                                        </Col>
                                        <Col>
                                            <Form.Label className='form-right'>
                                                {!!dataTicket ? dataTicket.description : ''}
                                            </Form.Label>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col lg='3' xs='3'>
                                            <Form.Label className='form-left'>
                                                Additional Info
                                            </Form.Label>
                                        </Col>
                                        <Col lg='1' xs='1' className='form-left'>
                                            :
                                        </Col>
                                        <Col>
                                            <Form.Label className='form-right'>
                                                {!!dataTicket ? dataTicket.additionalInfo : ''}
                                            </Form.Label>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col lg='3' xs='3'>
                                            <Form.Label className='form-left'>
                                                Attached file
                                            </Form.Label>
                                        </Col>
                                        <Col lg='1' xs='1' className='form-left'>
                                            :
                                        </Col>
                                        <Col>
                                            {/* <div className='form-right'> */}
                                                {!!dataTicket.Attachment ? 
                                                    !!dataTicket.Attachment[0] ? 
                                                        <a href={dataTicket.Attachment[0].url} className='form-right text-white' target='_blank' rel='noreferrer'>
                                                            Download
                                                        </a>
                                                        :
                                                        <></>
                                                :
                                                    <></>
                                                }
                                            {/* </div> */}
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col lg='3' xs='3'>
                                            <Form.Label className='form-left'>
                                                Assigned to
                                            </Form.Label>
                                        </Col>
                                        <Col lg='1' xs='1' className='form-left'>
                                            :
                                        </Col>
                                        <Col>
                                            <Form.Label className='form-right'>
                                                {!!dataTicket.assignedUser ? dataTicket.assignedUser.assignedName : <p className='text-muted fst-italic'>Not assigned</p>}
                                            </Form.Label>
                                        </Col>
                                    </Row>
                                </Card>
                            </Row>
                            <Row>
                                <p className='p-tittleManage'>Manage</p>
                            </Row>
                            <Form noValidate validated={validated} className='row card-resolve-ticket-wrapper' onSubmit={(e) => handleSaveData(e)}>
                                <Card className='card-form'>
                                    <Container>
                                            <Row className='form-group'>
                                                <Col lg={3} xs='3'>
                                                    <Form.Label>
                                                        Group
                                                    </Form.Label>
                                                </Col>
                                                <Col lg={1} xs='1'>
                                                    :
                                                </Col>
                                                <Col>
                                                    <Form.Select
                                                        className='form-select-resolve-ticket'
                                                        id="category"
                                                        // defaultValue={!!dataTicket ? dataTicket.category : ''}
                                                        // value = {inputData.category}
                                                        disabled={ role.replace('-', ' ').toLowerCase() !== 'super agent' || dataTicket.statusTicket === 'Solved' || dataTicket.statusTicket === 'Closed'}
                                                        onChange={ (e) => handleInput(e) }
                                                    >
                                                        {groups.map((group,i) => (
                                                            <option selected={group.label === dataTicket.category}>{group.label}</option>
                                                        ))}
                                                    </Form.Select>
                                                </Col>
                                            </Row>
                                            <Row className='form-group'>
                                                <Col xs='3'>
                                                    <Form.Label>
                                                        Status
                                                    </Form.Label>
                                                </Col>
                                                <Col xs='1'>
                                                    :
                                                </Col>
                                                <Col>
                                                    <Form.Select 
                                                        required
                                                        // isInvalid={!Boolean(inputData.statusTicket)}
                                                        className="me-sm-2 form-select-resolve-ticket"
                                                        // defaultValue={!!dataTicket ? dataTicket.statusTicket : ''}
                                                        id="statusTicket"
                                                        onChange={(e) => handleInput(e)}
                                                        disabled={dataTicket.statusTicket === 'Solved' || dataTicket.statusTicket === 'Closed'}
                                                        defaultValue=''
                                                    >
                                                        <option disabled value=''>Select</option>
                                                        {status.map((stat,i) => (
                                                            !['closed', 'open', 'reopen', 'on progress'].includes(lowerCase(stat.label)) && stat.label !== dataTicket.statusTicket ?
                                                                <option>{stat.label}</option>
                                                                :
                                                                <option hidden disabled>{stat.label}</option>
                                                        ))}
                                                        {/* <option value="0">On Progress</option>
                                                        <option value="1">Solved</option> */}
                                                    </Form.Select>
                                                    <Form.Control.Feedback type='invalid'>
                                                        You must specify a status
                                                    </Form.Control.Feedback>
                                                </Col>
                                            </Row>
                                            <Row className='form-group'>
                                                <Col xs='3'>
                                                    <Form.Label >
                                                        Response
                                                    </Form.Label>
                                                </Col>
                                                <Col xs='1'>
                                                    :
                                                </Col>
                                                <Col>
                                                    <Form.Control
                                                        required
                                                        id='response'
                                                        as="textarea"
                                                        placeholder="Type your response"
                                                        style={{ height: '100px' }}
                                                        value={defaultTo(inputData.response, '')}
                                                        disabled={dataTicket.statusTicket === 'Solved' || dataTicket.statusTicket === 'Closed'}
                                                        onChange={(e)=>handleInput(e)}
                                                    />
                                                    <Form.Control.Feedback type='invalid'>
                                                        Response is required
                                                    </Form.Control.Feedback>
                                                </Col>
                                            </Row>
                                    </Container>
                                </Card>
                                {!isEmpty(inputData) ? 
                                    <div className='btn-wrapper-resolve-ticket'>
                                        <Button
                                            variant='black'
                                            className='btn-cancel'
                                            onClick={handleCancel}
                                        >
                                            Cancel
                                        </Button>
                                        <Button type='submit' variant='orange' className='btn-save'>
                                            {isLoading ?
                                                (
                                                    <>
                                                    <span className='spinner-border spinner-border-sm'></span>
                                                    {'  '}Loading...
                                                    </>
                                                ) :
                                                "Save"
                                            }
                                        </Button>
                                    </div>
                                    :
                                    <></>
                                }
                            </Form>
                        </Col>
                        <Col className='col-logs'>
                            <Card className='card-logs'>
                            <Container>
                                <Row className='h5 logs-title'>
                                LOGS
                                </Row>

                            {!!ticketActivities ?
                                ticketActivities.map((ticketActivity) => {
                                    // change role to user when ticket status open/reopen, case for ticket created by agent
                                    if (['open', 'reopen'].includes(lowerCase(ticketActivity.ticketStatus))) ticketActivity.updatedByUserRole = 'user'
                                    return (
                                        <>
                                        <Row className={`d-flex align-items-center justify-content-center logs-${lowerCase(ticketActivity.updatedByUserRole).replace('super ','')}`}>
                                            <Row>
                                                {
                                                    //get additional info as log ticket when status open, response (agent)/comment(user) for other status
                                                    lowerCase(ticketActivity.ticketStatus) !== 'open' ? 
                                                    <div className='logs-text'>{defaultTo(ticketActivity.response || ticketActivity.comment, <div className='text-muted fst-italic'>-- No message --</div>)}</div>
                                                    :
                                                    <div className='logs-text'>{defaultTo(<><div>{dataTicket.description}</div><div>{dataTicket.additionalInfo}</div></>, <div className='text-muted fst-italic'>-- No message --</div>)}</div>
                                                }
                                                <div className='logs-style logs-text'>{`---${ticketActivity.updatedByUserName} (Status: ${ticketActivity.ticketStatus})` }</div>
                                            </Row>
                                            <Row className={`d-flex justify-content-between align-items-end ${lowerCase(ticketActivity.updatedByUserRole).replace('super ','') === 'agent' && 'flex-row-reverse justify-content-between'}`}>
                                                <div className={`date-logs-${lowerCase(ticketActivity.updatedByUserRole).replace('super ','')} text-muted logs-text`}>{dateTimeFormatter(new Date(ticketActivity.updatedAt))}</div>
                                            </Row>
                                        </Row>
                                        </>

                                    )
                                } 

                            )
                            :
                            <></>
                            }
                            </Container>
                            </Card>
                            
                        </Col>
                    </Row>
                </Container>
                <Modal id='modal-sucess-save-data' centered show={showModal} onHide={handleCloseModal} contentClassName='modal-paper-yellow'>
                    <Modal.Header className='modal-header-custom' closeButton />
                    <Modal.Body className='modal-group-body'>
                        <p>Save data success!</p>
                    </Modal.Body>
                    <Modal.Footer className='modal-footer-custom'>
                        <Button size='sm' className='btn-modal' onClick={() => handleCloseModal()}>
                            Close
                        </Button>
                    </Modal.Footer>
                </Modal>
            </body>
            <Footer/>
        </>
    )
}

export default ResolveTicket;
