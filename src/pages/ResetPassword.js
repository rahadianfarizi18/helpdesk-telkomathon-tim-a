//modules
import { useState, useRef } from "react";
import { Col, Container, Form, Row, Button, Modal, Alert } from "react-bootstrap";
import { useLocation } from "react-router-dom";
import { useDispatch } from "react-redux";
import { useForm } from "react-hook-form";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEye, faEyeSlash } from "@fortawesome/free-solid-svg-icons";
//redux
import { resetPassApi } from "../redux/auth/action";
//styles & assets
import styles from '../assets/scss/forgotPassword.module.scss'
import '../assets/scss/login.scss'
import logo from '../assets/logo/logo-solver.png'
import imgTriangle from '../assets/images/signin-left-triangle.png'
import CircleIllustration from '../assets/images/circle-illustration-login.svg'

const ResetPassword = () => {
    const location = useLocation()
    const { register, handleSubmit, watch, formState: {errors} } = useForm()
    const dispatch = useDispatch()
    const password = useRef({})
    password.current = watch('password', '')
    const token = new URLSearchParams(location.search).get('token')
    const username = new URLSearchParams(location.search).get('username')

    const [show, setShow] = useState(false);
    const [showAlert, setShowAlert] = useState(false)
    const [toggleShowPassword, setToggleShowPassword] = useState(false)
    const [toggleShowConfirmPassword, setToggleShowConfirmPassword] = useState(false)
    const [errorMessage, setErrorMessage] = useState('')

    const handleClose = () => setShow(false);

    const handleResetPassword = async (data) => {
        dispatch(resetPassApi({
            username: username,
            password: data.password,
            token,
        }))
        .then((res) => {
            setShow(true)
        })
        .catch(err => {
            setErrorMessage(err.message)
            setShowAlert(true)
        })
    }

    return(
        <>
        <div className='logo-wrapper-login'>
            <img alt='logo' src={logo} className='img-logo' width='200'/>
        </div>
        <div className='hr-wrapper'>
            <hr/>
            <hr/>
            <hr/>
            <hr/>
            <hr/>
        </div>
        <svg 
            data-src={CircleIllustration}
            width='120px'
            className={'circleIllustration-login'}
        />
        <img alt='illustration-triangle' src={imgTriangle} className='img-triangle' height='400px' />
        <div className='login-body'>
            <Container>
                <Alert variant='danger' show={showAlert}>
                    {errorMessage}
                </Alert>
                <Row className='form-root'>
                    <Col xs={6} className={styles.formImg}></Col>
                    <Col xs={6} className={styles.formPaper}>
                        <Container className='form-wrapper'>
                            <div className={`h4 ${styles.formTitle}`}>Reset Password</div>
                            <Form onSubmit={(e) => e.preventDefault()} >
                                <Form.Label className='label-login'>
                                    New Password
                                </Form.Label>
                                <div className='form-input-wrapper'>
                                    <Form.Control
                                        id='password'
                                        type={toggleShowPassword ? 'text' : 'password'}
                                        placeholder='enter new password'
                                        className='form-input'
                                        {...register('password', {
                                            required: {
                                                value: true,
                                                message: 'You must specify a password'
                                            },
                                            minLength: {
                                                value: 5,
                                                message: 'Password must have at least 5 characters'
                                            },
                                            maxLength: {
                                                value: 30,
                                                message: 'Password must less than 30 characters'
                                            },
                                            pattern : {
                                                value: /(?=.*\d)(?=.*[a-z])(?=.*[A-Z])/,
                                                message: 'Password must contain at least a lower case letter, an upper case letter, and a numeric'
                                            }
                                        })}    
                                    />
                                    <Link className='toggle-password-login' onClick={() => setToggleShowPassword(!toggleShowPassword)}><FontAwesomeIcon icon={toggleShowPassword ? faEyeSlash : faEye} /></Link>
                                </div>
                                {!!errors.password && <p className='form-text text-warning'>{errors.password.message}</p>}
                                <Form.Label className='label-login'>
                                    Confirm New Password
                                </Form.Label>
                                <div className='form-input-wrapper'>
                                    <Form.Control
                                        id='confirmPassword'
                                        name='confirmPassword'
                                        type={toggleShowConfirmPassword ? 'text' : 'password'}
                                        placeholder='confirm your new password'
                                        className='form-input' 
                                        {...register('confirmPassword', {
                                            validate: value => value === password.current 
                                        })}
                                    />
                                    <Link className='toggle-password-login' onClick={() => setToggleShowConfirmPassword(!toggleShowConfirmPassword)}><FontAwesomeIcon icon={toggleShowConfirmPassword ? faEyeSlash : faEye} /></Link>
                                </div>
                                
                                {!!errors.confirmPassword && <p className='form-text text-warning'>The passwords do not match</p>}
                                <div className={styles.buttonResetWrapper}>
                                <Button type='submit' size='sm' variant='orange' className='btn-login' onClick={handleSubmit(handleResetPassword)}>
                                    Reset
                                </Button>
                                </div>
                            </Form>
                        </Container>
                    </Col>
                </Row>
            </Container>

            <Modal show={show} onHide={handleClose} contentClassName={styles.modalCard} centered>
                    <Modal.Body className={styles.modalMessage}>Your New Password Has Been Set</Modal.Body>
                    <Modal.Footer className={styles.modalButtonFooter}> 
                        <Link to='/login'>
                            <Button variant="primary" onClick={handleClose} className={styles.modalButtonGoTo} >
                                Go to Login
                            </Button>
                        </Link>
                    </Modal.Footer>
             </Modal>
        </div>
        </>
    )
}

export default ResetPassword;