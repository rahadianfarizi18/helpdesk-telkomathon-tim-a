//modules
import { useEffect, useState } from "react";
import { Container, Row, Col, Button, Form, Alert } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPen, faTimes } from "@fortawesome/free-solid-svg-icons";
import DataTable from "react-data-table-component";
import { useDispatch, useSelector } from "react-redux";
import { get } from "lodash";
//components
import Header from "../components/Header";
import ModalConfirmation from "../components/ModalConfirmation";
//redux
import {
    addFaqApi,
    deleteFaqApi,
    editFaqApi,
    fetchFaqApi 
} from "../redux/faq/action";
//styles & assets
import styles from '../assets/scss/manageFAQ.module.scss'
import { manageFaqTableStyles } from "../assets/js/tableStyles";

const ManageFAQ = () => {

    const dispatch = useDispatch()
    const token = useSelector(states => states.auth.token)
    const faq = useSelector(states => states.faq.faqData)

    const [ showModalEditFAQ, setShowModalEditFAQ ] = useState(false)
    const [ showModalDeleteFAQ, setShowModalDeleteFAQ ] = useState(false)
    const [ isLoading, setIsLoading ] = useState(false)
    const [ isDataLoading, setIsDataLoading ] = useState(false)
    const [ showAlert, setShowAlert ] = useState(false)
    const [ showAlertFetchData, setShowAlertFetchData ] = useState(false)
    const [ isError, setIsError ] = useState(false)
    const [ alertMessage, setAlertMessage ] = useState('')
    const [ failFetchDataMessage, setFailFetchDataMessage ] = useState('')
    const [ inputData, setInputData ] = useState({})
    const [ isEditExisting, setIsEditExisting ] = useState(false)
    const [ faqToDelete, setFaqToDelete ] = useState({})

    const manageButton = (faqData) => {
        return(
            <>
            <Button className={styles.buttonManage} onClick={() => handleEditFAQ(faqData)}>
                <FontAwesomeIcon icon={faPen} size='xs'/>
            </Button>
            <Button variant='orange' className={styles.buttonManage} onClick={() => handleDeleteFAQ(faqData)}>
                <FontAwesomeIcon icon={faTimes} size='xs'/>
            </Button>
            </>
        )
    }

    const handleEditFAQ = (faqToEdit) => {
        setInputData(faqToEdit)
        setIsEditExisting(true)
        setShowModalEditFAQ(true)
    }

    const handleDeleteFAQ = (faqData) => {
        setShowModalDeleteFAQ(true)
        setFaqToDelete(faqData)
    }

    const handleConfirmDeleteFAQ = () => {

        const fetchFaq = () => {
            setIsDataLoading(true)
            dispatch(fetchFaqApi({token}))
            .then(res => {
                setIsDataLoading(false)
                setShowAlertFetchData(false)
            })
            .catch(err => {
                setIsDataLoading(false)
                setFailFetchDataMessage(err.message)
                setShowAlertFetchData(true)
            })
        }

        setIsLoading(true)
        dispatch(deleteFaqApi({token, faqId: faqToDelete.faqId}))
        .then(res => {
            setIsLoading(false)
            setIsError(false)
            setShowModalDeleteFAQ(false)
            fetchFaq()
            setShowAlert(true)
            setAlertMessage("Delete question success!")
        })
        .catch(err => {
            setIsLoading(false)
            setIsError(true)
            setShowModalDeleteFAQ(false)
            setShowAlert(true)
            setAlertMessage("Delete question failed! " + err.message)
        })
    }

    const tableHeader = [
        {
            name: 'No',
            selector: (row, index) => index+1,
            sortable: true,
            maxWidth: '20px',
        },
        {
            name: 'Question',
            selector: row => row.question,
            sortable: true,
            maxWidth: '26%',
            wrap: true
        },
        {
            name: 'Answer',
            selector: row => row.answer,
            wrap: true,
            sortable: true,
        },
        {
            name: 'Manage',
            selector: row => manageButton(row),
            sortable: true,
            maxWidth: '150px',
            wrap: true,
        },
    ]

    const handleCloseModal = () => {
        setIsEditExisting(false)
        setShowModalEditFAQ(false)
        setShowModalDeleteFAQ(false)
    }

    const handleSave = () => {

        const fetchFaq = () => {
            setIsDataLoading(true)
            dispatch(fetchFaqApi({token}))
            .then(res => {
                setIsDataLoading(false)
                setShowAlertFetchData(false)
            })
            .catch(err => {
                setIsDataLoading(false)
                setFailFetchDataMessage(err.message)
                setShowAlertFetchData(true)
            })
        }

        setIsLoading(true)
        if(isEditExisting) {
            delete inputData.createdAt
            delete inputData.updatedAt
            delete inputData.statusActive
            dispatch(editFaqApi({token, data: inputData}))
            .then(res => {
                setIsLoading(false)
                setIsError(false)
                setShowModalEditFAQ(false)
                fetchFaq()
                setShowAlert(true)
                setAlertMessage("Edit question success!")
            })
            .catch(err => {
                setIsLoading(false)
                setIsError(true)
                setShowModalEditFAQ(false)
                setShowAlert(true)
                setAlertMessage("Edit question failed! " + err.message)
            })
        }
        else {
            dispatch(addFaqApi({token, data: inputData}))
            .then(res => {
                setIsLoading(false)
                setIsError(false)
                setShowModalEditFAQ(false)
                fetchFaq()
                setShowAlert(true)
                setAlertMessage("Add question success!")
            })
            .catch(err => {
                setIsLoading(false)
                setIsError(true)
                setShowModalEditFAQ(false)
                setShowAlert(true)
                setAlertMessage("Add question failed! " + err.message)
            })
        }
    }

    const handleAddFAQ = () => {
        setInputData({})
        setShowModalEditFAQ(true)
    }

    useEffect(() => {
        
        const fetchFaq = () => {
            setIsDataLoading(true)
            dispatch(fetchFaqApi({token}))
            .then(res => {
                setIsDataLoading(false)
                setShowAlertFetchData(false)
            })
            .catch(err => {
                setIsDataLoading(false)
                setFailFetchDataMessage(err.message)
                setShowAlertFetchData(true)
            })
        }

        fetchFaq()

    }, [dispatch, token])

    const modalMessage =

        <Container>
            <div className={styles.modalTitle}>
                {isEditExisting ? 
                    "Edit FAQ"
                    :
                    "Add New FAQ"
                }
            </div>
            <Form.Label className={styles.formLabel}>Question</Form.Label>
            <Form.Control
                as='textarea'
                id='label'
                defaultValue={get(inputData, 'question', '')}
                placeholder='insert question here'
                onChange={(e) => setInputData({
                    ...inputData,
                    question: e.target.value,
                })}
                style={{ minHeight: '70px' }}
                className={styles.formInput}
            />  
            <Form.Label className={styles.formLabel}>Answer</Form.Label>
            <Form.Control
                as='textarea'
                defaultValue={get(inputData, 'answer', null)}
                placeholder='insert answer here'
                className={styles.formInput}
                onChange={(e) => setInputData({
                    ...inputData,
                    answer: e.target.value
                })}
                style={{ minHeight: '150px' }}
            />  
        </Container>

    return (
        <>
        <header>
            <Header />
        </header>
        <body className={styles.body}>
            <Container>
                <Alert show={showAlert} variant={isError? 'danger' : 'success'} id='fail-manage-category'>{alertMessage}</Alert>
                <Alert show={showAlertFetchData} variant='danger' id='fail-fetch-category'>{`Load categories data failed! ${failFetchDataMessage}`}</Alert>
                <Row>
                    <div className={`h3 ${styles.title}`}>Manage FAQ</div>
                </Row>
                <Row className='add-user-row justify-content-end'>
                    <Col xs='8' lg='8'>
                        { }
                    </Col>
                    <Col xs='auto' lg='auto'>
                        <button className='ms-auto btn-add-user' onClick={() => handleAddFAQ()}>+ Add New FAQ</button>
                    </Col>
                </Row>
                <Row>
                    {isDataLoading ? 
                        <div className='d-flex justify-content-center'>
                            <div className='spinner-border text-primary'>
                                <span className='visually-hidden'></span>
                            </div>
                        </div>
                        :
                        <div className='table-wrapper-manage-user'>
                            <DataTable 
                                responsive
                                fixedHeader
                                fixedHeaderScrollHeight = '52vh'
                                columns={tableHeader}
                                data = {faq}
                                customStyles = {manageFaqTableStyles}
                                highlightOnHover
                                pagination
                                paginationComponentOptions={{
                                    selectAllRowsItem: true,
                                }}
                            />
                        </div>
                    }
                </Row>
            </Container>
            <ModalConfirmation 
                showModal={showModalEditFAQ}
                handleCloseModal={()=>handleCloseModal()}
                message={modalMessage}
                handleConfirm={handleSave}
                isLoading={isLoading}
                saveButton='Save'
                cancelButton='Cancel'
            />
            <ModalConfirmation 
                showModal={showModalDeleteFAQ}
                handleCloseModal={()=>handleCloseModal()}
                message='You will delete this question. Continue?'
                handleConfirm={handleConfirmDeleteFAQ}
                isLoading={isLoading}
                variant="Yellow"
            />
        </body>
        </>
    )
}

export default ManageFAQ;