//modules
import { Container } from 'react-bootstrap';
import { Link } from 'react-router-dom'
//components
import Header from '../components/Header';
//styles & assets
import styles from '../assets/scss/notFound.module.scss'

const NotFound = () => {
    return (
        <>
            <header>
                <Header/>
            </header>
            <body className={styles.body}>
                <Container className={styles.root}>
                    <h1>Page Not Found</h1>
                    <Link to='/' exact>
                        <p>Back to home</p>
                    </Link>
                </Container>
            </body>
        </>
    )
}

export default NotFound;