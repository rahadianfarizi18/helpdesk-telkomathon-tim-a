import * as theme from './theme'

export const manageTicketTableStyles = {
    table: {
        style: {
            backgroundColor: 'none',
            borderRadius: '10px',
        }
    },
    headCells: {
        style: {
            backgroundColor: theme.primaryColor,
            color: 'white',
            fontFamily: 'sans-serif',
            fontSize: '11pt',
            fontWeight: 'bold',
            borderRight: `1px solid ${theme.backgroundColor}`,
            textOverflow: 'clip',
            // paddingRight: '.3rem',
            // paddingLeft: '.3rem',
            ':first-child': {
                borderTopLeftRadius: '1rem',
            },
            ':last-child': {
                borderTopRightRadius: '1rem',
            },
        },
        sortStyle: {
            '&:hover': {
                color: theme.yellow
            },
            '&:hover:focus': {
                color: theme.yellow
            },
            '&:hover:active': {
                color: theme.yellow
            },
        }
    },
    headRow: {
        style: {
            backgroundColor: 'none',
            minHeight: '36px',
        },
        denseStyle: {
            backgroundColor: theme.backgroundColor,
        }
    },
    rows: {
        denseStyle: {
            minHeight: '40px',
            ':last-child': {
                borderRadius: '0 0 1rem 1rem',
                borderBottom: '0 solid transparent'
            }
        }
    },
    cells: {
        style: {
            borderRight: `1px solid ${theme.backgroundColor}`,
            // whiteSpace: 'nowrap',
            width: 'auto',
            fontSize: '10pt'
        }
    },
    pagination: {
        style: {
            backgroundColor: theme.backgroundColor,
            color: theme.primaryColor,
            borderTop: '0 solid transparent'
        }
    }
}

export const manageUserTableStyles = {
    table: {
        style: {
            backgroundColor: 'none',
            borderRadius: '10px',
        }
    },
    headCells: {
        style: {
            backgroundColor: theme.primaryColor,
            color: 'white',
            fontFamily: 'sans-serif',
            fontSize: '11pt',
            fontWeight: 'bold',
            borderRight: `1px solid ${theme.backgroundColor}`,
            textOverflow: 'clip',
            // paddingRight: '.3rem',
            // paddingLeft: '.3rem',
            ':first-child': {
                borderTopLeftRadius: '1rem',
            },
            ':last-child': {
                borderTopRightRadius: '1rem',
            },
        },
        sortStyle: {
            '&:hover': {
                color: theme.yellow
            },
            '&:hover:focus': {
                color: theme.yellow
            },
            '&:hover:active': {
                color: theme.yellow
            },
        }
    },
    headRow: {
        style: {
            backgroundColor: theme.backgroundColor,
            minHeight: '36px',
        },
        denseStyle: {
            backgroundColor: theme.backgroundColor,
        }
    },
    rows: {
        denseStyle: {
            minHeight: '40px',
            ':last-child': {
                borderRadius: '0 0 1rem 1rem',
                borderBottom: '0 solid transparent'
            }
        }
    },
    cells: {
        style: {
            borderRight: `1px solid ${theme.backgroundColor}`,
            // whiteSpace: 'nowrap',
            width: 'auto',
            fontSize: '12pt',
            paddingTop: '.5rem',
            paddingBottom: '.5rem',
            alignItems: 'flex-start'
        }
    },
    pagination: {
        style: {
            backgroundColor: theme.backgroundColor,
            color: theme.primaryColor,
            borderTop: '0 solid transparent',
        }
    }
}

export const manageCategoriesTableStyles = {
    table: {
        style: {
            backgroundColor: 'none',
            borderRadius: '10px',
        }
    },
    headCells: {
        style: {
            backgroundColor: theme.primaryColor,
            color: 'white',
            fontFamily: 'sans-serif',
            fontSize: '11pt',
            fontWeight: 'bold',
            borderRight: `1px solid ${theme.backgroundColor}`,
            textOverflow: 'clip',
            // paddingRight: '.3rem',
            // paddingLeft: '.3rem',
            ':first-child': {
                borderTopLeftRadius: '1rem',
            },
            ':last-child': {
                borderTopRightRadius: '1rem',
            },
        },
        sortStyle: {
            '&:hover': {
                color: theme.yellow
            },
            '&:hover:focus': {
                color: theme.yellow
            },
            '&:hover:active': {
                color: theme.yellow
            },
        }
    },
    headRow: {
        style: {
            backgroundColor: theme.backgroundColor,
            minHeight: '36px',
        },
        denseStyle: {
            backgroundColor: theme.backgroundColor,
        }
    },
    rows: {
        denseStyle: {
            minHeight: '40px',
            ':last-child': {
                borderRadius: '0 0 1rem 1rem',
                borderBottom: '0 solid transparent'
            }
        }
    },
    cells: {
        style: {
            borderRight: `1px solid ${theme.backgroundColor}`,
            // whiteSpace: 'nowrap',
            width: 'auto',
            fontSize: '12pt',
            paddingTop: '.5rem',
            paddingBottom: '.5rem',
            alignItems: 'flex-start'
        }
    },
    pagination: {
        style: {
            backgroundColor: theme.backgroundColor,
            color: theme.primaryColor,
            borderTop: '0 solid transparent',
        }
    }
}

export const manageFaqTableStyles = {
    table: {
        style: {
            backgroundColor: 'none',
            borderRadius: '10px',
        }
    },
    headCells: {
        style: {
            backgroundColor: theme.primaryColor,
            color: 'white',
            fontFamily: 'sans-serif',
            fontSize: '11pt',
            fontWeight: 'bold',
            borderRight: `1px solid ${theme.backgroundColor}`,
            textOverflow: 'clip',
            // paddingRight: '.3rem',
            // paddingLeft: '.3rem',
            ':first-child': {
                borderTopLeftRadius: '1rem',
            },
            ':last-child': {
                borderTopRightRadius: '1rem',
            },
        },
        sortStyle: {
            '&:hover': {
                color: theme.yellow
            },
            '&:hover:focus': {
                color: theme.yellow
            },
            '&:hover:active': {
                color: theme.yellow
            },
        }
    },
    headRow: {
        style: {
            backgroundColor: theme.backgroundColor,
            minHeight: '36px',
        },
        denseStyle: {
            backgroundColor: theme.backgroundColor,
        }
    },
    rows: {
        denseStyle: {
            minHeight: '40px',
            ':last-child': {
                borderRadius: '0 0 1rem 1rem',
                borderBottom: '0 solid transparent'
            }
        }
    },
    cells: {
        style: {
            borderRight: `1px solid ${theme.backgroundColor}`,
            // whiteSpace: 'nowrap',
            width: 'auto',
            fontSize: '12pt',
            paddingTop: '.5rem',
            paddingBottom: '.5rem',
            alignItems: 'flex-start'
        }
    },
    pagination: {
        style: {
            backgroundColor: theme.backgroundColor,
            color: theme.primaryColor,
            borderTop: '0 solid transparent',
        }
    }
}