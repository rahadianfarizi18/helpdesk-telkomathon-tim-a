import Axios from 'axios'

export const FETCH_FAQ = 'FETCH_FAQ'

export const fetchFaqApi = ({token}) => (dispatch) => {
    const url = 'https://ticket-telkomathon.herokuapp.com/faq'
    const auth = {headers: {Authorization: token}}
    return new Promise ((resolve, reject) => {
        Axios.get(url, auth)
        .then(res => {
            if (res.data.code === 'error') {
                reject(res.data.message)
            }
            else {
                dispatch({type: FETCH_FAQ, data: res.data})
                resolve(res.data)
            }
        })
        .catch(err => {
            reject(err)
        })
    })
}

export const editFaqApi = ({token, data}) => () => {
    const url = 'https://ticket-telkomathon.herokuapp.com/faq'
    const auth = {headers: {Authorization: token}}
    return new Promise ((resolve, reject) => {
        Axios.put(url, data, auth)
        .then(res => {
            if (res.data.code === 'error') {
                reject(res.data)
            }
            else {
                resolve(res.data)
            }
        })
        .catch(err => {
            reject(err)
        })
    })
}

export const addFaqApi = ({token, data}) => () => {
    const url = 'https://ticket-telkomathon.herokuapp.com/faq'
    const auth = {headers: {Authorization: token}}
    const dataToAdd = {
        ...data,
        category: 'null',
    }
    return new Promise ((resolve, reject) => {
        Axios.post(url, dataToAdd, auth)
        .then(res => {
            if (res.data.code === 'error') {
                reject(res.data)
            }
            else {
                resolve(res.data)
            }
        })
        .catch(err => {
            reject(err)
        })
    })
}

export const deleteFaqApi = ({token, faqId}) => () => {
    const url = 'https://ticket-telkomathon.herokuapp.com/faq/' + faqId
    const auth = {headers: {Authorization: token}}
    return new Promise ((resolve, reject) => {
        Axios.delete(url, auth)
        .then(res => {
            if (res.data.code === 'error') {
                reject(res.data)
            }
            else {
                resolve(res.data)
            }
        })
        .catch(err => {
            reject(err)
        })
    })
}