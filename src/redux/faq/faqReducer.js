import { FETCH_FAQ } from "./action"

const initialState = {
    faqData: []
}

const faqReducer = (state = initialState, action) => {
    if(action.type === FETCH_FAQ) {
        state = {
            ...state,
            faqData: action.data
        }
    }
    return state;
}

export default faqReducer;