//modules
import Axios from "axios"
import { get, lowerCase } from "lodash"

//action types
export const FETCH_TICKET = 'FETCH_TICKET'
export const UPDATE_CURRENT_TICKET_ID = 'UPDATE_CURRENT_TICKET_ID'
export const UPDATE_CURRENT_TICKET = 'UPDATE_CURRENT_TICKET'
export const FETCH_STATUS_TICKET_REF = 'FETCH_STATUS_TICKET_REF'
export const COUNT_ALL_TICKETS = 'COUNT_ALL_TICKETS'
export const FETCH_TICKET_ACTIVITY = 'FETCH_TICKET_ACTIVITY'
export const FETCH_TICKET_REVIEW = 'FETCH_TICKET_REVIEW'
export const FETCH_COUNT_TICKET_STATUS = 'FETCH_COUNT_TICKET_STATUS'


export const countTicketByStatus = ({token, role, userId}) => (dispatch) => {
    
    let countOpenTickets = 0
    let countReopenTickets = 0
    let countDispatchedTickets = 0
    let countOnGoingTickets = 0
    let countSolvedTickets = 0
    let countClosedTickets = 0

    return new Promise((resolve, reject) => {
        dispatch(fetchTicketApi({token}))
        .then(data => {
            for(let ticket of data.rows) {
                if(lowerCase(ticket.statusTicket) === 'open' && (lowerCase(role) === 'super agent' || get(ticket.assignedUser, 'assignedUserId','') === '' )) countOpenTickets+=1
                if(lowerCase(ticket.statusTicket) === 'reopen' && (lowerCase(role) === 'super agent' || get(ticket.assignedUser, 'assignedUserId','') === '' )) countReopenTickets+=1
                if(lowerCase(ticket.statusTicket) === 'on progress' && (lowerCase(role) === 'super agent' || get(ticket.assignedUser, 'assignedUserId','') === userId )) countOnGoingTickets+=1
                if(lowerCase(ticket.statusTicket) === 'solved' && (lowerCase(role) === 'super agent' || get(ticket.assignedUser, 'assignedUserId','') === userId )) countSolvedTickets+=1
                if(lowerCase(ticket.statusTicket) === 'dispatched' && (lowerCase(role) === 'super agent' || get(ticket.assignedUser, 'assignedUserId','') === userId )) countDispatchedTickets+=1
                if(lowerCase(ticket.statusTicket) === 'closed' && (lowerCase(role) === 'super agent' || get(ticket.assignedUser, 'assignedUserId','') === userId )) countClosedTickets+=1
            }
            const countTicketsByStatus = {
                open: countOpenTickets,
                reopen: countReopenTickets,
                'on progress': countOnGoingTickets,
                solved: countSolvedTickets,
                dispatched: countDispatchedTickets,
                closed: countClosedTickets
            }
            dispatch({type: FETCH_COUNT_TICKET_STATUS, data: countTicketsByStatus})
            resolve(countTicketsByStatus)
        })
        .catch(err => {
            reject(err)
        })
    })

}

export const fetchTicketApi = ({token}) => (dispatch) => {
    let url = 'https://ticket-telkomathon.herokuapp.com/tickets'

    return new Promise ((resolve, reject) => {
        Axios.get(url, {
            headers: {
                Authorization: token
            }
        })
        .then(res => {
            dispatch({type: FETCH_TICKET, data: res.data.rows})
            dispatch({type: COUNT_ALL_TICKETS, data:res.data.count})
            resolve(res.data)
        })
        .catch(err => {
            reject(err)
        })

    })
}

export const updateCurrentTicket = (ticket) => ({
    type: UPDATE_CURRENT_TICKET,
    data: ticket
})

export const fetchDetailTicketApi = ({token, mainTicketId}) => (dispatch) => {
    const url = 'https://ticket-telkomathon.herokuapp.com/tickets'
    return new Promise ((resolve, reject) => {
        Axios.get(url + '/' + mainTicketId, {
            headers: {
                Authorization: token
            }
        })
        .then(res => {
            dispatch(updateCurrentTicket(res.data.ticketDetail))
            resolve(res.data.ticketDetail)
        })
        .catch(err => {
            reject(err)
        })

    })
}

export const fetchStatusTicketRefApi = (token) => (dispatch) => {
    const url = 'https://ticket-telkomathon.herokuapp.com/reference/statusTicket'
    return new Promise ((resolve, reject) => {
        Axios.get(url, {
            headers: {
                Authorization: token
            }
        })
        .then(res => {
            dispatch({type: FETCH_STATUS_TICKET_REF, data:res.data})
            resolve(true)
        })
        .catch(err => {
            reject(err)
        })
    })
}

export const updateTicketApi = ({token, data}) => (dispatch) => {
    const url = 'https://ticket-telkomathon.herokuapp.com/tickets'
    if (data.statusTicket === 'Dispatched') {
        data = {
            ...data,
            category: null,
            assignedUserId: null
        }
    }
    
    return new Promise((resolve, reject) => {
        Axios.put(url, data , {
            headers: {
                Authorization: token
            }
        })
        .then(res => {
            if(res.data.code === 'error') {
                reject(res.data)
            } else {
                resolve(res.data)
            }
        })
        .catch(err => reject(err))
    })
}

export const fetchTicketActivityApi = ({token, ticketId}) => (dispatch) => {
    const url = 'https://ticket-telkomathon.herokuapp.com/ticketActivity/'+ticketId
    return new Promise((resolve, reject) => {
        Axios.get(url, {
            headers: {
                Authorization: token
            }
        })
        .then(res => {
            if(res.data.code !== 'error') {
                dispatch({type:FETCH_TICKET_ACTIVITY,data:res.data})
                resolve(res.data)
            } else {
                reject(res.data)
            }
        })
        .catch(err => {
            reject(err)
        })
    })
}

export const fetchTicketReviewApi = ({token, ticketId}) => (dispatch) => {
    const url = 'https://ticket-telkomathon.herokuapp.com/review/ticket/' + ticketId
    return new Promise((resolve, reject) => {
        Axios.get(url, {
            headers: {
                Authorization: token
            }
        })
        .then(res => {
            if(res.data.code === 'error') {
                dispatch({type: FETCH_TICKET_REVIEW, data: {}})
                reject(res.data)
            } else {
                dispatch({type: FETCH_TICKET_REVIEW, data: res.data})
                resolve(res.data)
            }
        })
        .catch(err => {
            reject(err)
        })
    })
}
