//action types
import {
    FETCH_TICKET,  
    UPDATE_CURRENT_TICKET,
    UPDATE_CURRENT_TICKET_ID,
    FETCH_STATUS_TICKET_REF,
    COUNT_ALL_TICKETS,
    FETCH_TICKET_ACTIVITY,
    FETCH_TICKET_REVIEW,
    FETCH_COUNT_TICKET_STATUS,

} from "./action"

const initialState = {
    ticketsData: [],
    stats: [],
    currentTicketId: 0,
    currentTicket: {},
    allTicketsCount: 0,
    ticketActivity: [],
    ticketReview: {},
    countTicketsByStatus: []
}

const ticketsReducer = (state = initialState, action) => {
    if(action.type === FETCH_TICKET) {
        return {
            ...state,
            ticketsData: action.data
        }
    }
    if(action.type === FETCH_STATUS_TICKET_REF) {
        return {
            ...state,
            stats: action.data
        }
    }
    if(action.type === UPDATE_CURRENT_TICKET) {
        return {
            ...state,
            currentTicket: action.data
        }
    }
    if(action.type === UPDATE_CURRENT_TICKET_ID) {
        return {
            ...state,
            currentTicketId: action.data
        }
    }
    if(action.type === COUNT_ALL_TICKETS) {
        return {
            ...state,
            allTicketsCount: action.data
        }
    }
    if(action.type === FETCH_TICKET_ACTIVITY) {
        return {
            ...state,
            ticketActivity: action.data
        }
    }
    if(action.type === FETCH_TICKET_REVIEW) {
        return {
            ...state,
            ticketReview: action.data
        }
    }
    if(action.type === FETCH_COUNT_TICKET_STATUS) {
        return {
            ...state,
            countTicketsByStatus: action.data
        }
    }
    return state
}

export default ticketsReducer