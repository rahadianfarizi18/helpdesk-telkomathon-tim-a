//action types
import {
    FETCH_USERS,
    FETCH_ROLES,
    FETCH_GROUPS,
    COUNT_ALL_USERS
 } from "./action"

const initialState = {
    groups: [],
    roles: [],
    usersData: [],
    allUsersCount: 0,
}

const usersReducer = (state = initialState, action) => {
    if(action.type === FETCH_USERS) {
        return {
            ...state,
            usersData: action.data,
            // usersDataView: action.data,
        }
    }
    if(action.type === FETCH_ROLES) {
        return {
            ...state,
            roles: action.data,
        }
    }
    if(action.type === FETCH_GROUPS) {
        return {
            ...state,
            groups: action.data,
        }
    }
    if(action.type === COUNT_ALL_USERS) {
        return {
            ...state,
            allUsersCount: action.data,
        }
    }
    return state;
}

export default usersReducer;