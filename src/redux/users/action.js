//modules
import Axios from 'axios'

//action types
export const FETCH_USERS = "FETCH_USERS"
export const FETCH_ROLES = 'FETCH_ROLES'
export const FETCH_GROUPS = 'FETCH_GROUPS' 
export const COUNT_ALL_USERS = 'COUNT_ALL_USERS'

export const updateUser = (data) => ({
    type: FETCH_USERS,
    data: data
})

export const fetchUsers = ({token}) => (dispatch) => {
    let url = 'https://ticket-telkomathon.herokuapp.com/user'

    return new Promise((resolve, reject) => {
        Axios.get(url, {
            headers: {
                Authorization: `${token}`
            }
        })
        .then((res) => {
            dispatch({type: FETCH_USERS, data: res.data.rows})
            dispatch({type: COUNT_ALL_USERS, data: res.data.count})
            resolve(res.data)
        })
        .catch((err) => {
            reject(err)
        })
    })
}

export const fetchRoles = (token) => (dispatch) => {
    const toTitleCase = (str) => {
        let strCopy = str.slice()
        strCopy = strCopy.toLowerCase().replace('-', ' ')
        strCopy = strCopy.split(' ')
        for(let i=0; i<strCopy.length; i++) {
            strCopy[i] = strCopy[i].charAt(0).toUpperCase() + strCopy[i].slice(1)
        }
        strCopy = strCopy.join(' ')
        return(strCopy)
    }

    return new Promise ((resolve, reject) => {
        Axios.get('https://ticket-telkomathon.herokuapp.com/role', {
            headers : {
                Authorization: `${token}`
            }
        })
        .then((res) => {
            let rolesArr = [];
            for (let role of res.data) {
                if(role.role.toLowerCase() !== 'user') rolesArr.push(toTitleCase(role.role))
            }
            dispatch({type: FETCH_ROLES, data: rolesArr})
            resolve(true)
        })
        .catch((err) => {
            reject(err)
        })
    })
}

export const fetchGroups = (token) => (dispatch) => {
    return new Promise ((resolve, reject) => {
        Axios.get('https://ticket-telkomathon.herokuapp.com/reference/category', {
            headers : {
                Authorization: `${token}`
            }
        })
        .then((res) => {
            dispatch({type: FETCH_GROUPS, data: res.data})
            resolve(true)
        })
        .catch((err) => {
            reject(err)
        })
    })
}

export const addCategory = ({token, mainUserId, categoryId}) => (dispatch) => {
    const url = 'https://ticket-telkomathon.herokuapp.com/user/category'
    return new Promise ((resolve, reject) => {
        Axios.post(`${url}/${mainUserId}/${categoryId}`, null, {
            headers: {
                Authorization: `${token}`
            }
        })
        .then(res => {
            resolve(res)
        })
        .catch(err => {
            reject(err)
        })
    })
}

export const deleteCategory = ({token, mainUserId, categoryId}) => (dispatch) => {
    const url = 'https://ticket-telkomathon.herokuapp.com/user/category'
    return new Promise((resolve, reject) => {
        Axios.delete(`${url}/${mainUserId}/${categoryId}`, {
            headers: {
                Authorization: `${token}`
            }
        })
        .then(res => {
            resolve(res)
        })
        .catch(err => {
            reject(err)
        })
    })
}

export const changeRoleApi = ({token, userData}) => () => {
    const url = 'https://ticket-telkomathon.herokuapp.com/user'
    return new Promise((resolve, reject) => {
        Axios.put(url, userData, {
            headers: {
                Authorization: token
            }
        })
        .then(res => {
            resolve(res)
        })
        .catch(err => {
            reject(err)
        })
    })
}

export const addUserApi = ({token, userData}) => () => {
    const url = 'https://ticket-telkomathon.herokuapp.com/user'
    return new Promise((resolve, reject) => {
        Axios.post(url, userData, {
            headers: {
                Authorization: token
            }
        })
        .then(res => {
            resolve(res)
        })
        .catch(err => {
            reject(err)
        })
    })
}

export const deleteUserApi = ({token, uid}) => () => {
    const url = 'https://ticket-telkomathon.herokuapp.com/user'
    return new Promise((resolve, reject) => {
        Axios.delete(`${url}/${uid}`, {
            headers: {
                Authorization: token
            }
        })
        .then(res => {
            resolve(res)
        })
        .catch(err => {
            reject(err)
        })
    })
}
