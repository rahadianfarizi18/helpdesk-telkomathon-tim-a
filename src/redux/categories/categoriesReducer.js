import { FETCH_CATEGORIES } from './action'

const initialState = {
    categoriesData: []
}

const categoriesReducer = (state = initialState, action) => {
    if(action.type === FETCH_CATEGORIES) {
        state = {
            ...state,
            categoriesData: action.data
        }
    }
    return state
}

export default categoriesReducer