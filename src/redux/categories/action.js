import Axios from 'axios'

export const FETCH_CATEGORIES = 'FETCH_CATEGORIES'

export const fetchCategoriesApi = ({token}) => dispatch => {
    const url = 'https://ticket-telkomathon.herokuapp.com/category'
    const auth = {headers: {Authorization: token}}
    return new Promise ((resolve, reject) => {
        Axios.get(url, auth)
        .then(res => {
            if (res.data.code === 'error') {
                reject(res.data.message)
            }
            else {
                dispatch({type: FETCH_CATEGORIES, data: res.data})
                resolve(res.data)
            }
        })
        .catch(err => {
            reject(err)
        })
    })
}

export const editCategoryApi = ({token, data}) => () => {
    const url = 'https://ticket-telkomathon.herokuapp.com/category'
    const auth = {headers: {Authorization: token}}
    return new Promise ((resolve, reject) => {
        Axios.put(url, data, auth)
        .then(res => {
            if (res.data.code === 'error') {
                reject(res.data)
            }
            else {
                resolve(res.data)
            }
        })
        .catch(err => {
            reject(err)
        })
    })
}

export const addCategoryApi = ({token, data}) => () => {
    const url = 'https://ticket-telkomathon.herokuapp.com/category'
    const auth = {headers: {Authorization: token}}
    return new Promise ((resolve, reject) => {
        Axios.post(url, data, auth)
        .then(res => {
            if (res.data.code === 'error') {
                reject(res.data)
            }
            else {
                resolve(res.data)
            }
        })
        .catch(err => {
            reject(err)
        })
    })
}

export const deleteCategoryApi = ({token, categoryId}) => () => {
    const url = 'https://ticket-telkomathon.herokuapp.com/category/' + categoryId
    const auth = {headers: {Authorization: token}}
    return new Promise ((resolve, reject) => {
        Axios.delete(url, auth)
        .then(res => {
            if (res.data.code === 'error') {
                reject(res.data)
            }
            else {
                resolve(res.data)
            }
        })
        .catch(err => {
            reject(err)
        })
    })
}