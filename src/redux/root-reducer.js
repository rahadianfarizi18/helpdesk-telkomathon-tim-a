//modules
import { combineReducers } from "redux";
import storage from "redux-persist/lib/storage";

//reducers
import usersReducer from "./users/usersReducer";
import ticketsReducer from "./tickets/ticketsReducer";
import authReducer from "./auth/authReducer";
import categoriesReducer from "./categories/categoriesReducer";
import faqReducer from "./faq/faqReducer";

const appReducer = combineReducers({
    auth: authReducer,
    users: usersReducer,
    tickets: ticketsReducer,
    categories: categoriesReducer,
    faq: faqReducer,
})

const rootReducer = (state, action) => {
    if(action.type === 'LOGOUT') {
        storage.removeItem('persist:root') //clear persist when logout
        return appReducer(undefined, action)
    }
    return appReducer(state,action)
}

export default rootReducer;