//modules
import Axios from 'axios'

//action types
export const LOGIN = 'LOGIN'
export const FETCH_PROFILE = 'FETCH_PROFILE'

export const fetchProfile = ({userId, token}) => (dispatch) => {
    const url = 'https://ticket-telkomathon.herokuapp.com/user'
    return new Promise((resolve,reject) => {
        Axios.get(url + '/' + userId, {
            headers: {
                Authorization: token
            }
        })
        .then(res => {
            if(res.data.code === 'error') {
                reject(res.data)
            } else {
                dispatch({
                    type: FETCH_PROFILE,
                    data: res.data
                })
                resolve(true)
            }
        })
        .catch (err => {
            reject(err)
        })
    })
}

export const loginAPI = ({email, password}) => (dispatch) => {
    const loginData = {
        username: email,
        password
    }
    return new Promise((resolve, reject) => {
        Axios.post('https://ticket-telkomathon.herokuapp.com/auth/login', loginData)
        .then((res) => {
            let isLogin = false
            if(res.data.code === 'error') {
                reject(res.data)
            }
            if(res.data.token && res.data.role !== 'User') {
                isLogin = true
                dispatch({
                    type: LOGIN,
                    data: {...res.data, isLogin}
                })
                dispatch(fetchProfile({userId:res.data.userId, token: res.data.token}))
            } else if (res.data.token && res.data.role === 'User') {
                isLogin = false
                dispatch({
                    type: LOGIN,
                    data: {...res.data, isLogin}
                })
            }
            resolve(res.data)
        })
        .catch((err) => {
            reject(err)
        })
    })
}

export const forgotPassApi = (username) => () => {
    const url = 'https://ticket-telkomathon.herokuapp.com/auth/requestTokenResetPassword'
    return new Promise((resolve, reject) => {
        Axios.post(url, {username})
        .then(res => {
            if(res.data.code === 'error') {
                reject(res.data)
            } else {
                resolve(res.data)
            }
        })
        .catch(err => {
            reject(err)
        })
    })
}

export const resetPassApi = ({username, password, token}) => () => {
    const url = 'https://ticket-telkomathon.herokuapp.com/auth/resetPasswordUsingToken'
    const bodyData = {username, password, token}
    return new Promise((resolve, reject) => {
        Axios.put(url, bodyData)
        .then(res => {
            if(res.data.code === 'error') {
                reject(res.data)
            } else {
                resolve(res.data)
            }
        })
        .catch(err => {
            reject(err)
        })
    })
}

export const updateProfileApi = ({token, userId, userData}) => (dispatch) => {
    const url = 'https://ticket-telkomathon.herokuapp.com/user'
    return new Promise((resolve, reject) => {
        Axios.put(url, userData, {
            headers: {
                Authorization: token
            }
        })
        .then(res => {
            if(res.data.code === 'error') {
                reject(res.data)
            } else {
                resolve(res.data)
            }
        })
        .catch(err => {
            reject(err)
        })
    })
}