//action types
import { LOGIN, FETCH_PROFILE } from "./action";

const initialState = {
    username: '',
    role: '',
    userId: 0,
    token: '',
    isLogin: false,
    profile: {},
}

const authReducer = (state = initialState, action) => {
    if(action.type === LOGIN) {
        return {
            ...state,
            username: action.data.username,
            role: action.data.role,
            userId: action.data.userId,
            token: action.data.token,
            isLogin: action.data.isLogin,
        }
    }
    if(action.type === FETCH_PROFILE) {
        return {
            ...state,
            profile: action.data
        }
    }
    return state;
}

export default authReducer;