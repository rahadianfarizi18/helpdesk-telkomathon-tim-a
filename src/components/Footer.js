import { Container, Card, Col, Row } from "react-bootstrap"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faCopyright } from "@fortawesome/free-regular-svg-icons"
import '../assets/scss/footer.scss'


const Footer = () => {
    return(
        <footer>
            <div className='footer-responsive-text'>
                <FontAwesomeIcon icon={faCopyright} /> Solver by Telkom Indonesia
            </div>
            <Container className='container-footer'>

                <Row className='row-footer'>
                <Col xs='6' md='6' className='col-about'>
                    <h3>About Solver</h3>
                    <h6 className='text-about'>Solver merupakan Paltform Aplikasi yang menyediakan layanan
                        Ticketing terkait kendala. Baik yang terkait dengan operasional maupun Dashboard Telkom.
                    </h6>
                </Col>

                <Col xs='6' md='6' className='col-address'>
                    <Row className='h3 tittle'>Solver by Telkom Indonesia</Row>
                    <Row className='h6 text'>Jalan Jendral Gatot Subroto, Kav-52 <br/>
                    Jakarta Selatan DKI Jakarta <br/>
                    (021) 7123456</Row> 
                </Col>
                </Row>
                <div className='half-circle' />
            </Container>
        </footer>
    )
}

export default Footer;