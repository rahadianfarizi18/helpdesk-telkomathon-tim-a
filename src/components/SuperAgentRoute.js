//protect route for logged in users as super agent

import { Route, Redirect } from 'react-router-dom'
import { useSelector } from 'react-redux'

const SuperAgentRoute = ({component: Component, ...rest}) => {
    const isLogin = useSelector((states) => states.auth.isLogin)
    const role = useSelector(states => states.auth.role)

    return (
        <Route
            {...rest}
            render={({ location, ...routeProps }) =>
                isLogin && role.replace('-', ' ').toLowerCase() === 'super agent' ? (
                <Component {...routeProps}/>
                ) : (
                <Redirect
                    to={{
                    pathname: "/",
                    state: { from: location }
                    }}
                />
                )
            }
        />
    );
}


// const stateToProps = (globalState) => {
//     return {
//         isLogin: globalState.isLogin,
//     };
// };

export default SuperAgentRoute;