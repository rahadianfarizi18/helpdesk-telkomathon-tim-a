import { Navbar, Nav, DropdownButton, Dropdown } from "react-bootstrap"
import { NavLink, Redirect, useHistory } from "react-router-dom"
import { get } from "lodash"
import '../assets/scss/navbar.scss'
import logo from '../assets/logo/logo-solver-w.png'
import { useDispatch, useSelector } from "react-redux"

const Header = () => {
    const dispatch = useDispatch()
    const history = useHistory()
    const role = useSelector(states => get(states.auth,'role',''))
    const profile = useSelector(states => get(states.auth, 'profile', ''))

    const handleLogout = () => {
        dispatch({type:'LOGOUT'})
        return (
            <Redirect to='/login' />
        )
    }
    return(
        <>
            <Navbar fixed='top' collapseOnSelect expand='lg' className='mb-3 navbar-root'>
                <NavLink className='navbar-brand text-white' to='/'><img alt='logo' width='50%' src={logo} /></NavLink>
                <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                <Navbar.Collapse id="responsive-navbar-nav">
                    <Nav className='ms-auto navbar-items'>
                        <NavLink activeClassName='selected' to='/' exact className='link'>
                            Home
                        </NavLink>
                        <NavLink activeClassName='selected' to='/manage-tickets' className='link'>
                            Manage Tickets
                        </NavLink>
                        {role.replace('-', ' ').toLowerCase() === "super agent" ? 
                            (
                                <>
                                    <NavLink activeClassName='selected' to='/manage-user' className='link'>
                                        Manage Users
                                    </NavLink>
                                    <NavLink activeClassName='selected' to='/manage-categories' className='link'>
                                        Manage Categories
                                    </NavLink>
                                    <NavLink activeClassName='selected' to='/manage-faq' className='link'>
                                        Manage FAQ
                                    </NavLink>
                                </>
                            )
                            :
                            <></>
                        }
                        <DropdownButton variant='my-account' size='sm' align='end' title='My Account' className='link shadow'>
                            <Dropdown.ItemText key='name' >{profile.name}</Dropdown.ItemText>
                            <Dropdown.Divider className='divider' />
                            <Dropdown.Item key='manage-account' onClick={() => history.push('/manage-account')}>Manage account</Dropdown.Item>
                            <Dropdown.Item key='logout' onClick={handleLogout}>
                                Logout
                            </Dropdown.Item>
                        </DropdownButton>
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
        </>
    )
}

export default Header