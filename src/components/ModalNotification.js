import React from 'react'
import { Modal, Button } from "react-bootstrap";
import styles from '../assets/scss/modal.module.scss'

const ModalNotification = (props) => {
    const {showModal, handleCloseModal, message } = props
    
    return (
        <Modal id='modal-sucess-assign-group' centered show={showModal} onHide={handleCloseModal} contentClassName={styles.modalPaperYellow}>
            <Modal.Header className='modal-header-custom' closeButton />
            <Modal.Body className='modal-group-body'>
                <p>{message}</p>
            </Modal.Body>
            <Modal.Footer className='modal-footer-custom'>
                <Button size='sm' className='btn-modal' onClick={handleCloseModal}>
                    Close
                </Button>
            </Modal.Footer>
        </Modal>
    )
}

export default ModalNotification;