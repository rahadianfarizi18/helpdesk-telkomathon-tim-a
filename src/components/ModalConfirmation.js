import React from 'react'
import { Modal, Button } from "react-bootstrap";
import styles from '../assets/scss/modal.module.scss'

const ModalConfirmation = (props) => {
    const {showModal, handleCloseModal, handleConfirm, message, isLoading, saveButton, cancelButton, variant } = props
    const paperStyle = variant ? `modalPaper${variant}` : "modalPaper"
    return (
        <Modal centered show={showModal} onHide={handleCloseModal} contentClassName={styles[paperStyle]}>
            <Modal.Header className={styles.modalHeader} closeButton />
            <Modal.Body className={styles.modalBody}>
                <p>{message}</p>
            </Modal.Body>
            <Modal.Footer className={styles.modalFooter}>
                <Button size='sm' variant='orange' className={styles.btnModal} onClick={handleConfirm}>
                    {isLoading ?
                        (
                            <>
                            <span className='spinner-border spinner-border-sm'></span>
                            {'  '}Loading...
                            </>
                        ) :
                        saveButton || "Yes"
                    }
                </Button>
                <Button size='sm' className={styles.btnModal} onClick={handleCloseModal}>
                    {cancelButton || "No"}
                </Button>
            </Modal.Footer>
        </Modal>
    )
}

export default ModalConfirmation;