//protect route for logged in users

import { Route, Redirect } from 'react-router-dom'
import { useSelector } from 'react-redux'

const PrivateRoute = ({component: Component, ...rest}) => {
    const isLogin = useSelector((states) => states.auth.isLogin)
    return (
        <Route
            {...rest}
            render={({ location, ...routeProps }) =>
                isLogin ? (
                <Component {...routeProps}/>
                ) : (
                <Redirect
                    to={{
                    pathname: "/login",
                    state: { from: location }
                    }}
                />
                )
            }
        />
    );
}

export default PrivateRoute;