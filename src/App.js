//modules
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

//styles & assets
import './assets/scss/customBootstrap.scss'
import './App.css';

//pages
import Home from './pages/Home';
import Login from './pages/Login';
import ForgotPassword from './pages/ForgotPassword'
import ResetPassword from './pages/ResetPassword';
import ManageUser from './pages/ManageUser';
import NewUser from './pages/NewUser';
import ResolveTicket from './pages/ResolveTicket';
import NotFound from './pages/NotFound';
import ManageTickets from './pages/ManageTickets';
import ManageAccount from './pages/ManageAccount';
import ManageCategories from './pages/ManageCategories';
import ManageFAQ from './pages/ManageFAQ.js';

//components
import PrivateRoute from './components/PrivateRoute';
import SuperAgentRoute from './components/SuperAgentRoute';


function App() {

  return (
    <Router>
        <Switch>
            <PrivateRoute path='/' exact component={Home} />
            <Route path='/login' exact component={Login} />
            <Route path='/forgot-password' exact component={ForgotPassword} />
            <Route path='/reset-password' exact component={ResetPassword} />
            <SuperAgentRoute path='/manage-user' exact component={ManageUser} />
            <SuperAgentRoute path='/new-user' exact component={NewUser} />
            <SuperAgentRoute path='/manage-categories' exact component={ManageCategories} />
            <SuperAgentRoute path='/manage-faq' exact component={ManageFAQ} />
            <PrivateRoute path='/resolve-ticket' exact component={ResolveTicket} />
            <PrivateRoute path='/manage-tickets' exact component={ManageTickets} />
            <PrivateRoute path='/manage-account' exact component={ManageAccount} />
            <Route path='*' component={NotFound} />
        </Switch>
    </Router>
  );
}

export default App;
