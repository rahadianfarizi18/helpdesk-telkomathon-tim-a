import { useState } from "react";
import { useEffect } from "react";
import Header from "../components/Header";
import Footer from "../components/Footer";
import { Container, Row, Col, Form, Table, Modal, Button, Spinner } from "react-bootstrap";
import { Link } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { addCategory, changeRoleApi, deleteCategory, deleteUserApi, fetchGroups, fetchRoles, fetchUsers, updateUser, updateUserView } from "../redux/users/action";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTrashAlt, faTimes, faPlusCircle } from "@fortawesome/free-solid-svg-icons";
// import Select from "react-select";
import '../assets/scss/manageUser.scss'

const ManageUser = () => {

    const defaultLimitPage = 5

    const token = useSelector((states) => states.auth.token)

    const dispatch = useDispatch()
    const groups = useSelector((states) => states.users.groups)
    const roles = useSelector((states) => states.users.roles)
    const users = useSelector((states) => states.users.usersData)
    const countUsers = useSelector(states => states.users.allUsersCount)

    const [filter, setFilter] = useState({})
    const [isLoading, setIsLoading] = useState(false)
    const [isUserDataLoading, setIsUserDataLoading] = useState(false)
    const [showModal, setShowModal] = useState(false)
    // const [groupsOptions, setGroupsOptions] = useState([])
    const [currentUserGroups, setCurrentUserGroups] = useState('')
    const [showModalAssignGroup, setShowModalAssignGroup] = useState(false)
    const [showModalDeleteGroup, setShowModalDeleteGroup] = useState(false)
    const [showModalDeleteUser, setShowModalDeleteUser] = useState(false)
    const [showModalSuccessDeleteUser, setShowModalSuccessDeleteUser] = useState(false)
    const [showModalFailDeleteUser, setShowModalFailDeleteUser] = useState(false)
    const [selectedCategoryId, setSelectedCategoryId] = useState(0)
    const [categoryToDelete, setCategoryToDelete] = useState()
    const [userIdToAddCategory, setUserIdToAddCategory ] = useState()
    const [userToDeleteCategory, setUserToDeleteCategory ] = useState()
    const [userIdToDelete, setUserIdToDelete ] = useState()
    const [userToChangeRole, setUserToChangeRole] = useState()
    const [roleChangeInput, setRoleChangeInput] = useState()
    const [limitPage, setLimitPage] = useState(defaultLimitPage)
    const [page, setPage] = useState(1)
    const [lastPage, setLastPage] = useState(1)
 
    const toTitleCase = (str) => {
        let strCopy = str.slice()
        strCopy = strCopy.toLowerCase().replace('-', ' ')
        strCopy = strCopy.split(" ")
        for(let i=0; i<strCopy.length; i++) {
            strCopy[i] = strCopy[i].charAt(0).toUpperCase() + strCopy[i].slice(1)
        }
        strCopy = strCopy.join(' ')
        return (strCopy)
    }
    
    const handleCloseModal = () => {
        setShowModal(false)
        setShowModalAssignGroup(false)
        setShowModalDeleteGroup(false)
        setShowModalDeleteUser(false)
        setShowModalSuccessDeleteUser(false)
        setShowModalFailDeleteUser(false)
    }
                    
    const handleDeleteGroup = (e, user, usersCategory) => {
        e.preventDefault();
        setCategoryToDelete(usersCategory)
        setUserToDeleteCategory(user)
        setShowModalDeleteGroup(true)
        
    }

    const handleConfirmDeleteGroup = async (e) => {
        e.preventDefault();
        setIsLoading(true)
        await dispatch(deleteCategory({
            token,
            mainUserId: userToDeleteCategory.mainUserId,
            categoryId: categoryToDelete.categoryId,
        }))
        .then( async() => {
            setIsLoading(false)
            handleCloseModal()
            // setIsUserDataLoading(true)
            dispatch(fetchUsers({token, limitPage, page}))
            // .then(() => {
            //     setIsUserDataLoading(false)
            // })
            // .catch(()=> {
            //     setIsUserDataLoading(false)
            // })
        })
        .catch(()=> {
            setIsLoading(false)
            handleCloseModal()
        })
    }

    const handleChangeRole = (e, user) => {
        setUserToChangeRole(user)
        setRoleChangeInput(e.target.value)
        setShowModal(true)
    }

    const handleConfirmChangeRole = async(e) => {
        e.preventDefault()
        setIsLoading(true)
        let category = []
        for (let userCategory of userToChangeRole.usersCategories) {
            category.push(userCategory.categoryId)
        }

        const userEdited = {
            ...userToChangeRole,
            category,
            role: roleChangeInput,
        }
        delete userEdited.mainUserId
        delete userEdited.usersCategories
        delete userEdited.jobLocation
        delete userEdited.description
        await dispatch(changeRoleApi({token, userData: userEdited}))
        .then(() => {
            setIsLoading(false)
            handleCloseModal()
            dispatch(fetchUsers({token, limitPage, page}))
        })
        .catch(() => {
            setIsLoading(false)
            handleCloseModal()
        })

    }

    const handleChangeFilter = (e) => {
        setLimitPage(countUsers)
        setFilter({
            ...filter,
            [e.target.id]: e.target.value,
        })
    }

    const handleAddGroup = (e, user) => {
        e.preventDefault();
        setShowModalAssignGroup(true)
        setUserIdToAddCategory(user.mainUserId)
        let userGroup = [];
        for(let data of user.usersCategories) {
            userGroup.push(data.category.name)
        }
        setCurrentUserGroups(userGroup)
    }

    const handleSelectCategory = (e) => {
        setSelectedCategoryId(e.target.value)
    }

    const handleConfirmAddGroup = async (e) => {
        // e.preventDefault()
        setIsLoading(true)
        await dispatch(addCategory({
            token,
            mainUserId: userIdToAddCategory,
            categoryId: selectedCategoryId,
        }))
        .then(async () => {
            setIsLoading(false)
            handleCloseModal()
            // setIsUserDataLoading(true)
            dispatch(fetchUsers({token, limitPage, page}))
            // .then(() => {
            //     setIsUserDataLoading(false)
            // })
            // .catch(()=> {
            //     setIsUserDataLoading(false)
            // })
        })
        .catch(() => {
            setIsLoading(false)
            handleCloseModal()
        })
    }

    const handleDeleteUser = (e, uid) => {
        e.preventDefault();
        setShowModalDeleteUser(true)
        setUserIdToDelete(uid)
    }

    const handleConfirmDeleteUser = async(e) => {
        e.preventDefault();
        setIsLoading(true)
        await dispatch(deleteUserApi({token, uid: userIdToDelete}))
        .then(() => {
            setShowModalDeleteUser(false)
            setShowModalSuccessDeleteUser(true)
            dispatch(fetchUsers({token, limitPage, page}))
            setIsLoading(false)
        })
        .catch(() => {
            setShowModalFailDeleteUser(true)
            setIsLoading(false)
        })
    }

    useEffect(() => {

        const fetchUsersData = () => {
            setIsUserDataLoading(true)
            dispatch(fetchUsers({token, limitPage, page}))
            .then(() => {
                setIsUserDataLoading(false)
            })
        }

        const fetchRolesData = () => {
            dispatch(fetchRoles(token))
        }

        const fetchGroupsData = () => {
            dispatch(fetchGroups(token))
        }

        setLastPage(Math.ceil(countUsers/limitPage))


        fetchUsersData()
        fetchRolesData()
        fetchGroupsData()


    }, [countUsers, dispatch, limitPage, page, token])


    return (
        <>
            <header>
                <Header />
            </header>
            <body className='body-manage-user'>
                <Container>
                    <Row>
                        <div className='title h3'>Manage Users</div>
                    </Row>
                    <Row className='justify-content-center filter-root'>
                        <Col xs='6' lg='2' className='filter'>
                            <Row>
                                Nama
                            </Row>
                            <Row>
                                <Form.Control id='name' className='input-text' placeholder='enter user name' onChange={(e) => handleChangeFilter(e)}/>
                            </Row>
                        </Col>
                        <Col xs='6' lg='2' className='filter'>
                            <Row>Group</Row>
                            <Row>
                                <Form.Select id='group' className='input-select' onChange={(e) => handleChangeFilter(e)} >
                                    <option>All</option>
                                    {groups.map((group, i) => (
                                        <option key={i}>{group.category}</option>
                                    ))}
                                </Form.Select>
                                {/* <Select
                                    options={groupsOptions}
                                    id='group'
                                    className='input-select'
                                    onChange={(e) => handleChangeFilter(e)}
                                /> */}
                            </Row>
                        </Col>
                        <Col xs='6' lg='2' className='filter'>
                            <Row>Role</Row>
                            <Row>
                                <Form.Select id='role' className='input-select' onChange={(e) => handleChangeFilter(e)}>
                                    <option>All</option>
                                    {roles.map((role, i) => (
                                        <option key={i}>{role}</option>
                                    ))}
                                </Form.Select>
                            </Row>
                        </Col>
                    </Row>
                    <Row className='add-user-row justify-content-end'>
                        <Col xs='8' lg='8'>
                            { }
                        </Col>
                        <Col xs='auto' lg='auto'>
                            <Link to='/new-user'>
                                <button className='ms-auto btn-add-user'>Add User +</button>
                            </Link>
                        </Col>
                    </Row>
                    <Row>
                        <div className='table-wrapper-manage-user'>
                            <Table responsive className='table'>
                                <thead>
                                    <tr className='table-head'>
                                        <th className='left-head'>User Identity</th>
                                        {/* <th>NIK</th>
                                        <th>E-Mail</th> */}
                                        <th>Division/Position</th>
                                        {/* <th>Unit</th> */}
                                        <th className='fit'>Role</th>
                                        <th className='groups-head'>Group(s)</th>
                                        <th className='fit'>Delete</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {isUserDataLoading ? 
                                            <div className='d-flex justify-content-center'>
                                                <div className='spinner-border text-primary'>
                                                    <span className='visually-hidden'></span>
                                                </div>
                                            </div>

                                        : 
                                        users.filter((user, i) => {
                                            const userName = user.name.toLowerCase()
                                            let userGroups = []
                                            for (let category of user.usersCategories) {
                                                userGroups.push(category.category.label)
                                            }
                                            const userRoles = user.role
                                            let nameFilterRes;
                                            try {
                                                nameFilterRes = userName.includes(filter.name.toLowerCase())
                                            }
                                            catch {
                                                nameFilterRes = true
                                            }
                                            const groupFilterRes = filter.group && filter.group !== 'All' ? userGroups.includes(filter.group) : true
                                            const roleFilterRes = filter.role && filter.role !== 'All' ? userRoles.replace('-', ' ').toLowerCase() === filter.role.replace('-', ' ').toLowerCase() : true
                                            return (
                                                nameFilterRes && groupFilterRes && roleFilterRes
                                        )}).map((user,idxUser) => (
                                            <tr>
                                                <td>
                                                    <div className='user-name'>{user.name}</div>
                                                    <div className='user-nik'>{user.employeeNumber}</div>
                                                    <div className='user-email'>{user.username}</div>
                                                </td>
                                                {/* <td>{user.nik}</td>
                                                <td>{user.email}</td> */}
                                                <td>
                                                    <div className='user-division'>{user.division}</div>
                                                    <div className='user-position'>{user.position}</div>
                                                </td>
                                                {/* <td>{user.unit}</td> */}
                                                <td className='fit'>
                                                    <Form.Select value={toTitleCase(user.role)} className='btn-yellow select-role' onChange={(e) => handleChangeRole(e, user)}>
                                                        {roles.map((role,i) => (
                                                            <option>{role}</option>
                                                        ))}
                                                    </Form.Select>
                                                </td>
                                                <td key={idxUser}>{user.usersCategories.map((usersCategory,idxGroup) => {
                                                    const idx = {idxUser, idxGroup} 
                                                    return (
                                                        <button disabled className='btn-groups'>
                                                            {usersCategory.category.name}
                                                            <Link className='close-button' onClick={(e) => handleDeleteGroup(e, user, usersCategory)}>
                                                                <FontAwesomeIcon icon={faTimes} size='xs'/>
                                                            </Link>
                                                        </button>
                                                )},user)}
                                                    <button className='delete-icon' onClick={(e) => handleAddGroup(e, user)}>
                                                        <FontAwesomeIcon icon={faPlusCircle} />
                                                    </button>
                                                </td>
                                                <td className='fit'>
                                                    <center>
                                                        <button className='delete-icon' disabled={user.role.replace('-', ' ').toLowerCase() === 'super agent'} onClick={(e) => handleDeleteUser(e, user.mainUserId)}>
                                                            <FontAwesomeIcon icon={faTrashAlt} />
                                                        </button>
                                                    </center>
                                                </td>
                                            </tr>
                                        ))}
                                </tbody>

                            </Table>

                        </div>
                    </Row>
                    <Row className='justify-content-between'>
                        <Col xs='6' lg='6' className='select-limit-manage-user'>
                            showing
                            <Form.Select size='sm' value={limitPage} onChange={(e) => setLimitPage(e.target.value)}>
                                <option>5</option>
                                <option>10</option>
                                <option>20</option>
                                <option value={countUsers}>All</option>
                            </Form.Select> of {countUsers} users
                        </Col>
                        <Col xs='auto' lg='auto' className='prev-next-root'>
                            <Link className='prev-next-btn' onClick={() => setPage(1)}>First </Link>
                            <Link className='prev-next-btn' onClick={() => page > 1 ? setPage(page-1) : setPage(page)}>{'< '}</Link>
                            {`  page ${page} of ${lastPage}  `}
                            <Link className='prev-next-btn' onClick={() => page > 1 ? setPage(page+1) : setPage(page)}>{' >'}</Link>
                            <Link className='prev-next-btn' onClick={() => setPage(lastPage)}> Last</Link>
                        </Col>
                    </Row>
                </Container>
                <Modal id='modal-change-role' centered show={showModal} onHide={handleCloseModal} contentClassName='modal-paper'>
                    <Modal.Header className='modal-header-custom' closeButton />
                    <Modal.Body>
                        <p>You will make a change of role to this user.<br/> Continue?</p>
                    </Modal.Body>
                    <Modal.Footer className='modal-footer-custom'>
                        <Button size='sm' variant='orange' className='btn-modal' onClick={(e) => handleConfirmChangeRole(e)}>
                            {isLoading ?
                                (
                                    <>
                                    <span className='spinner-border spinner-border-sm'></span>
                                    {'  '}Loading...
                                    </>
                                ) :
                                "Confirm"
                            }
                        </Button>
                    </Modal.Footer>
                </Modal>
                <Modal id='modal-assign-group' centered show={showModalAssignGroup} onHide={handleCloseModal} contentClassName='modal-paper-yellow'>
                    <Modal.Header className='modal-header-custom' closeButton />
                    <Modal.Body className='modal-group-body'>
                            <p className='modal-group-title'>Assign to a Group</p>
                            <Form.Select defaultValue='Select' className='modal-group-select' onChange={(e) => handleSelectCategory(e)}>
                                <option disabled>Select</option>
                                {groups.map((group, i) => (
                                    currentUserGroups.includes(group.category) ? 
                                        (<></>)
                                        :
                                        (<option value={group.categoryId}>{group.category}</option>)
                                ))}
                            </Form.Select>
                    </Modal.Body>
                    <Modal.Footer className='modal-footer-custom'>
                        <Button disabled={selectedCategoryId === 0} size='sm' className='btn-modal' onClick={(e) => handleConfirmAddGroup(e)}>
                            {isLoading ?
                                (
                                    <>
                                    <span className='spinner-border spinner-border-sm'></span>
                                    {'  '}Loading...
                                    </>
                                ) :
                                "Assign"
                            }
                        </Button>
                    </Modal.Footer>
                </Modal>
                <Modal id='modal-delete-group' centered show={showModalDeleteGroup} onHide={handleCloseModal} contentClassName='modal-paper-yellow'>
                    <Modal.Header className='modal-header-custom' closeButton />
                    <Modal.Body className='modal-group-body'>
                    <p>Confirm delete group?</p>
                    </Modal.Body>
                    <Modal.Footer className='modal-footer-custom'>
                        <Button size='sm' className='btn-modal' onClick={(e) => handleConfirmDeleteGroup(e)}>
                        {isLoading ?
                            (
                                <>
                                <span className='spinner-border spinner-border-sm'></span>
                                {'  '}Loading...
                                </>
                            ) :
                            "Confirm"
                        }
                        </Button>
                    </Modal.Footer>
                </Modal>
                <Modal id='modal-delete-user' centered show={showModalDeleteUser} onHide={handleCloseModal} contentClassName='modal-paper-yellow'>
                    <Modal.Header className='modal-header-custom' closeButton />
                    <Modal.Body className='modal-group-body'>
                    <p>You will delete all data of this user. Continue?</p>
                    </Modal.Body>
                    <Modal.Footer className='modal-footer-custom'>
                        <Button size='sm' className='btn-modal' onClick={(e) => handleConfirmDeleteUser(e)}>
                        {isLoading ?
                            (
                                <>
                                <span className='spinner-border spinner-border-sm'></span>
                                {'  '}Loading...
                                </>
                            ) :
                            "Confirm"
                        }
                        </Button>
                    </Modal.Footer>
                </Modal>
                <Modal id='modal-sucess-delete-user' centered show={showModalSuccessDeleteUser} onHide={handleCloseModal} contentClassName='modal-paper-yellow'>
                    <Modal.Header className='modal-header-custom' closeButton />
                    <Modal.Body className='modal-group-body'>
                        <p>Delete user success!</p>
                    </Modal.Body>
                    <Modal.Footer className='modal-footer-custom'>
                        <Button size='sm' className='btn-modal' onClick={() => handleCloseModal()}>
                            Close
                        </Button>
                    </Modal.Footer>
                </Modal>
            </body>
            <footer>
                <Footer />
            </footer>
        </>
    )
}

export default ManageUser;