import Header from "../components/Header";
import Footer from "../components/Footer";
import styles from '../assets/scss/manageTickets.module.scss'
import illustration from '../assets/images/manage-tickets.png'
import { Col, Container, Row, Form, Table, Button, Modal, Spinner, Alert } from "react-bootstrap";
import { Link, useHistory } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { useState, useEffect } from "react";
import { fetchGroups } from "../redux/users/action";
import { fetchStatusTicketRefApi, fetchTicketApi, updateTicketApi } from "../redux/tickets/action";

const ManageTickets = () => {
    const defaultLimitPage = 10

    const dispatch = useDispatch()
    const history = useHistory()

    const [filter, setFilter] = useState({})
    const [isTicketsDataLoading, setIsTicketsDataLoading] = useState(false)
    const [isLoading, setIsLoading] = useState(false)
    const [showModalProcessTicket, setShowModalProcessTicket] = useState(false)
    const [showModalAssignGroup, setShowModalAssignGroup] = useState(false)
    const [showModalSuccessAssignGroup, setShowModalSuccessAssignGroup] = useState(false)
    const [showAlert, setShowAlert] = useState(false)
    const [errorMessage, setErrorMessage] = useState(false)
    const [ticketUpdate, setTicketUpdate] = useState({})
    const [limitPage, setLimitPage] = useState(defaultLimitPage)
    const [ticketIdUpdateGroup, setTicketIdUpdateGroup] = useState(0)
    const [page, setPage] = useState(1)
    const [lastPage, setLastPage] = useState(1)
    const [categoryToAssign, setCategoryToAssign] = useState('')

    const groups = useSelector((states) => states.users.groups)
    const stats = useSelector((states) => states.tickets.stats)
    const tickets = useSelector((states) => states.tickets.ticketsData)
    const countTickets = useSelector(states => states.tickets.allTicketsCount)
    const {token, role, userId} = useSelector(states => states.auth)

    const handleChangeFilter = (e) => {
        setLimitPage(countTickets)
        setFilter({
            ...filter,
            [e.target.id]: e.target.value
        })
        // setLimitPage(tempLimitPage)
    }

    const handleProcessTicket = (ticketId) => {
        const data = {
            ticketId,
            assignedUserId: userId,
            statusTicket: 'On Progress'
        }
        setTicketUpdate(data)
        

        setShowModalProcessTicket(true)
    }

    const handleConfirmProcessTicket = () => {
        
        const fetchTickets = async () => {
            setIsTicketsDataLoading(true)
            await dispatch(fetchTicketApi({token, limitPage, page}))
            .then(() => setIsTicketsDataLoading(false))
            .catch(err => { 
                setErrorMessage(`Get ticket failed. ${err.message}`)
                setShowAlert(true)
                setIsTicketsDataLoading(false)
            })
        }
        
        setIsLoading(true)
        dispatch(updateTicketApi({token, data: ticketUpdate}))
        .then((res) => {
            setIsLoading(false)
            fetchTickets()
            handleCloseModal()
        })
        .catch((err) => {
            setIsLoading(false)
            handleCloseModal()
        })
    }

    const handleAssignGroup = (e, ticketId) => {
        setTicketIdUpdateGroup(ticketId)
        setShowModalAssignGroup(true)
    }

    const handleConfirmAssignGroup = async() => {
        // await dispatch(updateTicketApi)

        const fetchTickets = async () => {
            setIsTicketsDataLoading(true)
            await dispatch(fetchTicketApi({token, limitPage, page}))
            .then(() => setIsTicketsDataLoading(false))
            .catch(err => { 
                setErrorMessage(`Get ticket failed. ${err.message}`)
                setShowAlert(true)
                setIsTicketsDataLoading(false)
            })
        }

        setIsLoading(true)
        const data = {
            ticketId: ticketIdUpdateGroup,
            category: categoryToAssign,
            statusTicket: 'Open'
        }
        dispatch(updateTicketApi({token, data}))
        .then(res => {
            if (res.code !== 'success') {
                setShowModalAssignGroup(false)
                setErrorMessage(res.message)
                setShowAlert(true)
                setIsLoading(false)

            } else {
                setShowModalAssignGroup(false)
                setShowModalSuccessAssignGroup(true)
                setIsLoading(false)
                fetchTickets()
            }
        })
        .catch(err => {
            setShowModalAssignGroup(false)
            setErrorMessage(err.message)
            setShowAlert(true)
            setIsLoading(false)
        })
    }

    const handleCloseModal = () => {
        setShowModalProcessTicket(false)
        setShowModalAssignGroup(false)
        setShowModalSuccessAssignGroup(false)
    }

    const handleDetailTicket = (e, ticketId) => {
        e.preventDefault();
        // dispatch(updateCurrentTicketId(ticketId))
        // history.push(`/resolve-ticket/${ticketId}`)
        const location = {
            pathname: '/resolve-ticket',
            state: { ticketId }
        }
        history.push(location)
        // return (
        //     <Redirect
        //         to= {{
        //             pathname: '/resolve-ticket',
        //             state: { ticketId }
        //         }}
        //     />
        // )
    }


    const manageButton = (ticket) => {
        try {
            if(ticket.statusTicket.toLowerCase() === 'open' && !!!ticket.assignedUser) {
                return <Button size='sm' variant='orange' className={styles.buttonManage} onClick={() => handleProcessTicket(ticket.ticketId)}>
                            Process ticket
                        </Button>
            } else if (ticket.statusTicket.toLowerCase() === 'dispatched') {
                return <Button size='sm' variant='yellow' className={styles.buttonManage} onClick={(e) => handleAssignGroup(e, ticket.ticketId)}>
                           Assign to group
                        </Button>
            } else {
                return <Button size='sm' variant='black' className={styles.buttonManage} onClick={(e) => handleDetailTicket(e, ticket.ticketId)}>Go to ticket</Button>
            }


            // switch (status.statusTicket) {
            //     case 'OPEN':
            //         return <Button size='sm' variant='orange' className={styles.buttonManage} onClick={handleProcessTicket}>
            //                     Process ticket
            //                 </Button>
            //         // break;
            //     case null:
            //         return <Button size='sm' variant='yellow' className={styles.buttonManage} onClick={handleAssignGroup}>
            //                     Assign to group
            //                 </Button>
            //         // break;
            //     default:
            //         return <Button size='sm' variant='black' className={styles.buttonManage}>Go to ticket</Button>
            // }   
        }
        catch(err) {
            return  <Button size='sm' variant='black' className={styles.buttonManage} onClick={(e) => handleDetailTicket(e, ticket.ticketId)} >Go to ticket</Button>
        }
    }
    
    useEffect(() => {

        const fetchTickets = async () => {
            setIsTicketsDataLoading(true)
            await dispatch(fetchTicketApi({token, limitPage, page}))
            .then(() => setIsTicketsDataLoading(false))
            .catch(err => { 
                setErrorMessage(`Get ticket failed. ${err.message}`)
                setShowAlert(true)
                setIsTicketsDataLoading(false)
            })
        }

        const fetchStatusTicket = async () => {
            await dispatch(fetchStatusTicketRefApi(token))
            .then(data => {
            })
            .catch(err => {
            })
        }

        setLastPage(Math.ceil(countTickets/limitPage))

        fetchTickets()
        fetchStatusTicket()
        dispatch(fetchGroups(token))


    }, [countTickets, dispatch, limitPage, page, role, token, userId])

    return (
        <>
            <header>
                <Header />
            </header>
            <body className={styles.body}>
                <Container>
                    <Alert show={showAlert} variant='danger' id='fail-get-ticket'>{errorMessage}</Alert>
                    <Row>
                        <div className={`h3 ${styles.title}`}>Manage Tickets</div>
                    </Row>
                    <Row className={styles.filterRoot}>
                        <Col xs='6' lg='2' className={styles.filter}>
                            <Form.Label>Ticket ID</Form.Label>
                            <Form.Control id='id' className={`input-text ${styles.inputFilter}`} placeholder='enter ticket ID' onChange={(e) => handleChangeFilter(e)}/>
                        </Col>
                        <Col xs='6' lg='2' className={styles.filter}>
                            <Form.Label>Status</Form.Label>
                            <Form.Select id='status' className={`input-select ${styles.inputFilter}`} onChange={(e) => handleChangeFilter(e)}>
                                <option>All</option>
                                {stats.map((stat, i) => (
                                    <option key={i}>{stat.label}</option>
                                    )
                                )}
                            </Form.Select>
                        </Col>
                        <Col xs='6' lg='2' className={styles.filter}>
                            { role.replace('-', ' ').toLowerCase() === 'super agent' ? 
                                <>
                                    <Form.Label>Group</Form.Label>
                                    <Form.Select id='group' className={`input-select ${styles.inputFilter}`} onChange={(e) => handleChangeFilter(e)} >
                                        <option>All</option>
                                        {groups.map((group, i) => (
                                            <option key={i}>{group.label}</option>
                                        ))}
                                    </Form.Select>
                                </>
                                :
                                <></>
                            }
                        </Col>
                    </Row>
                    <Row>
                        <Col sm={12} lg={9}>
                            <Row>
                                <div className={styles.tableWrapper}>
                                    <Table size='sm' responsive>
                                        <thead>
                                            <tr className='table-head'>
                                                <th className={`fit ${styles.colId}`}>Ticket ID</th>
                                                {/* <th>NIK</th>
                                                <th>E-Mail</th> */}
                                                <th>Status</th>
                                                {/* <th>Unit</th> */}
                                                <th className={styles.colGroup}>Group</th>
                                                <th>Agent</th>
                                                <th className={`fit right-head ${styles.rightHead}`}>Manage</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {isTicketsDataLoading ? 
                                                <Spinner className='spinner-border m-5 text-center' variant='primary'>
                                                    <span className='visually-hidden'></span>
                                                </Spinner> 
                                                :
                                                (tickets.filter((ticket, i) => {
                                                    const ticketId = ticket.ticketId
                                                    const ticketGroup = ticket.category ? ticket.category : ''    

                                                    const ticketIdRes = Number(filter.id) ? ticketId === Number(filter.id) : true
                                                    let statusFilterRes
                                                    try {
                                                        const ticketStatus = ticket.statusTicket
                                                        statusFilterRes = filter.status && filter.status !== 'All' ? ticketStatus === filter.status : true
                                                    }
                                                    catch {
                                                        statusFilterRes = !filter.status || filter.status === 'All'
                                                    }
                                                    const groupFilterRes = filter.group && filter.group !== 'All' ? ticketGroup === filter.group : true
                                                    return (
                                                        ticketIdRes && statusFilterRes && groupFilterRes
                                                    )})
                                                .map((ticket,i) => (
                                                    <tr>
                                                        <td className='align-middle'>{ticket.ticketId}</td>
                                                        <td className='align-middle fit'>
                                                            {ticket.statusTicket ? 
                                                                ticket.statusTicket
                                                                :
                                                                <div className='text-muted'>Null</div>}
                                                        </td>
                                                        {
                                                            ticket.category && ticket.statusTicket !== 'Dispatched' ?
                                                            <td className={`fit align-middle`}>{ticket.category}</td>
                                                            :
                                                            <td className='align-middle text-muted fst-italic'>Not assigned</td>
                                                        }
                                                        {
                                                            //!!ticket.assignedUser && ticket.statusTicket !== 'Dispatched' && ticket.statusTicket !== 'Open' ?
                                                            !!ticket.assignedUser ?
                                                                <td className='align-middle fit'>
                                                                    {/* {users.filter((user) => {
                                                                        return(
                                                                            user.userId === ticket.assignedUserId
                                                                        )
                                                                    }).name} */}
                                                                    {ticket.assignedUser.assignedName}
                                                                </td>
                                                                :
                                                                <td className='align-middle text-muted fst-italic'>Not assigned</td>
                                                        }
                                                        <td className='align-middle fit'>
                                                            {/* <Button size='sm'>Manage</Button> */}
                                                            {manageButton(ticket)}
                                                        </td>
                                                    </tr>
                                                )))
                                            }
                                        </tbody>
                                    </Table>
                                </div>
                            </Row>
                            <Row className='justify-content-between'>
                                <Col xs='6' lg='6' className={styles.selectLimit}>
                                    showing
                                    <Form.Select size='sm' value={ limitPage } onChange={(e) => setLimitPage(e.target.value)}>
                                        <option>10</option>
                                        <option>20</option>
                                        <option>50</option>
                                        <option value={countTickets}>All</option>
                                    </Form.Select> of {countTickets} tickets
                                </Col>
                                <Col xs='auto' lg='auto' className='prev-next-root'>
                                    <Link className='prev-next-btn' onClick={() => setPage(1)}>First </Link>
                                    <Link className='prev-next-btn' onClick={() => page > 1 ? setPage(page-1) : setPage(page)}>{'< '}</Link>
                                    {`  page ${page} of ${lastPage}  `}
                                    <Link className='prev-next-btn' onClick={() => setPage(page+1)}>{' >'}</Link>
                                    <Link className='prev-next-btn' onClick={() => setPage(lastPage)}> Last</Link>
                                </Col>
                            </Row>
                        </Col>
                        <Col lg={3} sm={0} className={styles.colImg}>
                            <img alt='illustration' src={illustration} height='70%' />
                        </Col>
                    </Row>
                </Container>
                <Modal id='modal-process-ticket' centered show={showModalProcessTicket} onHide={handleCloseModal} contentClassName={styles.modalPaper}>
                    <Modal.Header className='modal-header-custom' closeButton />
                    <Modal.Body>
                        <p>You will be assigned to handle this group. Continue?</p>
                    </Modal.Body>
                    <Modal.Footer className='modal-footer-custom'>
                        <Button size='sm' variant='orange' className='btn-modal' onClick={handleConfirmProcessTicket}>
                            {isLoading ?
                                (
                                    <>
                                    <span className='spinner-border spinner-border-sm'></span>
                                    {'  '}Loading...
                                    </>
                                ) :
                                "Yes"
                            }
                        </Button>
                        <Button size='sm' className='btn-modal' onClick={handleCloseModal}>No</Button>
                    </Modal.Footer>
                </Modal>
                <Modal id='modal-assign-group' centered show={showModalAssignGroup} onHide={handleCloseModal} contentClassName={styles.modalPaperYellow}>
                    <Modal.Header className='modal-header-custom' closeButton />
                    <Modal.Body className={styles.modalGroupBody}>
                            <p className={styles.modalGroupTitle}>Assign to a Group</p>
                            <Form.Select className={styles.modalGroupSelect} defaultValue='Select' onChange={(e) => setCategoryToAssign(e.target.value)}>
                                <option disabled>Select</option>
                                {groups.map((group, i) => (
                                    <option key={group.categoryId}>{group.category}</option>
                                ))}
                            </Form.Select>
                    </Modal.Body>
                    <Modal.Footer className='modal-footer-custom'>
                        <Button size='sm' className='btn-modal' onClick={handleConfirmAssignGroup}>
                            {isLoading ?
                                (
                                    <>
                                    <span className='spinner-border spinner-border-sm'></span>
                                    {'  '}Loading...
                                    </>
                                ) :
                                "Assign"
                            }
                        </Button>
                    </Modal.Footer>
                </Modal>
                <Modal id='modal-sucess-assign-group' centered show={showModalSuccessAssignGroup} onHide={handleCloseModal} contentClassName='modal-paper-yellow'>
                    <Modal.Header className='modal-header-custom' closeButton />
                    <Modal.Body className='modal-group-body'>
                        <p>Ticket <span className={styles.modalSuccessAsignText}>{ticketIdUpdateGroup}</span> is assigned to <span className={styles.modalSuccessAsignText}>{categoryToAssign}</span></p>
                    </Modal.Body>
                    <Modal.Footer className='modal-footer-custom'>
                        <Button size='sm' className='btn-modal' onClick={() => handleCloseModal()}>
                            Close
                        </Button>
                    </Modal.Footer>
                </Modal>
            </body>
            <Footer />
        </>
    )
}

export default ManageTickets;